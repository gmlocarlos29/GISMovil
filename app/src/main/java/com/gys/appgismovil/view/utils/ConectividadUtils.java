package com.gys.appgismovil.view.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by USER on 29/08/2017.
 */
public class ConectividadUtils
{
    public static Boolean conectadoWifi(Context contexto)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null)
        {
            NetworkInfo info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if(info!=null)
            {
                if(info.isConnected())
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean conectadoRedMovil(Context contexto)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null)
        {
            NetworkInfo info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if(info!=null)
            {
                if(info.isConnected())
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean conectadoInternet(Context contexto)
    {

        if( conectadoWifi(contexto) || conectadoRedMovil(contexto))
            return true;
        else
            return false;
    }

    public static void MostrarAlertaSinConexion(Context context)
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        pDialog.setTitleText("Error de Conexión");
        pDialog.setContentText("Revise su conexión a Internet");
        pDialog.show();
    }

}
