package com.gys.appgismovil.view.activity.comun;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.LoginBody;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 23/08/2017.
 */
public class LoginActivity extends AppCompatActivity {

    ProgressDialog progressDialog = null;

    EditText txtRUC;
    EditText txtUsuario;
    EditText txtContrasenia;
    CheckBox chkRecordarContrasenia;
    Button btnIngresar;
    TextView lblDesarrolladoPor;

    TextInputLayout tilRUC;
    TextInputLayout tilUsuario;
    TextInputLayout tilContrasenia;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        IniciarControles();
        IniciarEventos();
        MostrarUsuarioPrueba();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Mostramos el usuario recordado
        Usuario objUsuarioRecordado = AutenticacionUtils.getUsuarioRecordado(this);
        if(objUsuarioRecordado!=null)
        {
            txtRUC.setText(objUsuarioRecordado.getUsu_docempresa());
            txtUsuario.setText(objUsuarioRecordado.getUsu_username());
            txtContrasenia.setText(objUsuarioRecordado.getUsu_password());
            chkRecordarContrasenia.setChecked(true);
        }
        txtRUC.requestFocus();
    }

    public void IniciarControles()
    {
        txtRUC = (EditText) findViewById(R.id.cmnLogin_txtRUC);
        tilRUC = (TextInputLayout)findViewById(R.id.cmnLogin_tilRUC);
        txtUsuario = (EditText) findViewById(R.id.cmnLogin_txtUsuario);
        tilUsuario = (TextInputLayout)findViewById(R.id.cmnLogin_tilUsuario);
        txtContrasenia = (EditText) findViewById(R.id.cmnLogin_txtContrasenia);
        tilContrasenia = (TextInputLayout)findViewById(R.id.cmnLogin_tilContrasenia);
        chkRecordarContrasenia = (CheckBox)findViewById(R.id.cmnLogin_chkRecordarContraseña);
        btnIngresar = (Button)findViewById(R.id.cmnLogin_btnIngresar);
        lblDesarrolladoPor = (TextView)findViewById(R.id.cmnLogin_lblDesarrolladoPor);
        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
    }

    public void IniciarEventos()
    {
        txtRUC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                esRUCValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        txtUsuario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                esUsuarioValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        txtContrasenia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                esContraseniaValido(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    private boolean esRUCValido(String nombre)
    {
        if (nombre.length() >= 8 && nombre.length() <= 11)
        {
            txtRUC.setError(null);
        }
        else if(nombre.length() == 0)
        {
            txtRUC.setError("Completa este campo");
            return false;
        }
        else if (nombre.length() < 8)
        {
            txtRUC.setError("Aumenta la longitud del texto a 8 caracteres como mínimo");
            return false;
        }
        return true;
    }

    private boolean esUsuarioValido(String nombre)
    {
        if (nombre.length() >= 4)
        {
            txtUsuario.setError(null);
        }
        else if(nombre.length() == 0)
        {
            txtUsuario.setError("Completa este campo");
            return false;
        }
        else if(nombre.length() < 4)
        {
            txtUsuario.setError("Aumenta la longitud del texto a 4 caracteres como mínimo");
            return false;
        }
        return true;
    }

    private boolean esContraseniaValido(String nombre)
    {
        if (nombre.length() >= 4)
        {
            txtContrasenia.setError(null);
        }
        else if(nombre.length() == 0)
        {
            txtContrasenia.setError("Completa este campo");
            return false;
        }
        else if(nombre.length() < 4)
        {
            txtContrasenia.setError("Aumenta la longitud del texto a 4 caracteres como mínimo");
            return false;
        }
        return true;
    }

    private void MostrarUsuarioPrueba()
    {
        txtRUC.setText("10452364927");
        txtUsuario.setText("cperez");
        txtContrasenia.setText("1234");
    }

    public void IniciarSesion(View view)
    {
        if(ConectividadUtils.conectadoInternet(this))
        {
            if(ValidarDatosAutenticarUsuario())
            {
                btnIngresar.setEnabled(false);

                final SweetAlertDialog pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                pDialog.setTitleText("Autenticando...");
                pDialog.setCancelable(false);
                pDialog.show();

                //Obtenemos los datos del usuario a autenticar
                String documento = txtRUC.getText().toString();
                String usuario = txtUsuario.getText().toString();
                String password = txtContrasenia.getText().toString();

                Call<Usuario> loginCall = mRESTApi.Autenticar(new LoginBody(documento,usuario,password));
                loginCall.enqueue(new Callback<Usuario>() {
                    @Override
                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {

                        btnIngresar.setEnabled(true);

                        if(response.isSuccessful() && response.body()!=null)
                        {
                            Usuario objResult = response.body();

                            if(objResult.Mensaje.Status == eStatus.Success)
                            {
                                // Guardamos los datos del usuario logueado en SharedPreferences
                                AutenticacionUtils.setUsuarioAutenticado(LoginActivity.this, objResult);

                                // Si se selecciono la opcion Recordar Contrasenia
                                // guardamos el ruc, usuario y contrasenia en SharedPreferences
                                if(chkRecordarContrasenia.isChecked())
                                {
                                    Usuario objUsuarioRecordado = new Usuario();
                                    objUsuarioRecordado.setUsu_docempresa(txtRUC.getText().toString());
                                    objUsuarioRecordado.setUsu_username(txtUsuario.getText().toString());
                                    objUsuarioRecordado.setUsu_password(txtContrasenia.getText().toString());
                                    AutenticacionUtils.setUsuarioRecordado(LoginActivity.this,objUsuarioRecordado);
                                }
                                else
                                {
                                    AutenticacionUtils.BorrarUsuarioRecordado(LoginActivity.this);
                                }

                                //Limpiamos todos los controles
                                LimpiarCamposLogin();

                                pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                pDialog.setTitleText("Ingreso Correcto");
                                pDialog.setContentText(objResult.getUsu_nombre()+", Ingresaste como "+objResult.getUsu_funcion()+
                                        " de la empresa "+objResult.getUsu_nomempresa());
                                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        pDialog.dismiss();
                                    }
                                }, 3000);

                                Intent myIntent = new Intent(getApplicationContext(), PrincipalActivity.class);
                                startActivity(myIntent);
                            }
                            else if(objResult.Mensaje.Status == eStatus.Empty)
                            {
                                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                pDialog.setTitleText("Error de Ingreso");
                                pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                                //pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                            }
                            else if(response.body().Mensaje.Status == eStatus.Error)
                            {
                                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                pDialog.setTitleText("Ocurrió un Error");
                                pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Usuario> call, Throwable t) {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(t.getMessage());
                        btnIngresar.setEnabled(true);
                    }
                });
            }
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(this);
        }
    }

    public void LimpiarCamposLogin()
    {
        txtRUC.setText("");
        txtRUC.setError(null);
        txtUsuario.setText("");
        txtUsuario.setError(null);
        txtContrasenia.setText("");
        txtContrasenia.setError(null);
        chkRecordarContrasenia.setChecked(false);
    }

    public boolean ValidarDatosAutenticarUsuario()
    {
        String ruc = tilRUC.getEditText().getText().toString();
        String usuario = tilUsuario.getEditText().getText().toString();
        String contrasenia = tilContrasenia.getEditText().getText().toString();

        boolean a = esRUCValido(ruc);
        boolean b = esUsuarioValido(usuario);
        boolean c = esContraseniaValido(contrasenia);

        return a&b&c;
    }

}
