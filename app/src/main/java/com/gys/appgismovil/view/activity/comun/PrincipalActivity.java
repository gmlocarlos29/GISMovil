package com.gys.appgismovil.view.activity.comun;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.InicioPrueba;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.util.Autenticacion;
import com.gys.appgismovil.view.fragment.categoriaproducto.CategoriaProductoPrincipalFragment;
import com.gys.appgismovil.view.fragment.cliente.ClientePrincipalFragment;
import com.gys.appgismovil.view.fragment.comun.InicioPrincipalFragment;
import com.gys.appgismovil.view.fragment.proveedor.ProveedorPrincipalFragment;
import com.gys.appgismovil.view.fragment.usuario.UsuarioPrincipalFragment;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConstantesUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by USER on 23/08/2017.
 */
public class PrincipalActivity extends AppCompatActivity{

    //Instancia del drawer
    private DrawerLayout drawerLayout;

    //Titulo inicial del drawer
    private String drawerTitle;

    CircleImageView imgAvatarUsuario;

    NavigationView navigationView;

    public static final String EXTRA_USUARIO_ID = "extra_usuario_id";
    public static final String EXTRA_CLIENTE_ID = "extra_cliente_id";
    public static final String EXTRA_PROVEEDOR_ID = "extra_proveedor_id";
    public static final String EXTRA_CATEGORIAPRODUCTO_ID = "extra_categoriaproducto_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        setToolbar(); // Setear Toolbar como action bar

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Obtenemos los widgets del navheader
        View header = navigationView.getHeaderView(0);

        //Obtenemos el usuario logueado
        //Usuario objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(this);
        InicioPrueba objUsuarioInicioPrueba = Autenticacion.getUsuarioInicioPrueba(this);

        TextView nombreusuario = (TextView)header.findViewById(R.id.navheader_nombreusuario);
        //nombreusuario.setText(objUsuarioAutenticado.getUsu_nombre());
        nombreusuario.setText(objUsuarioInicioPrueba.getNombreApellidos());

        TextView funcionusuario = (TextView)header.findViewById(R.id.navheader_funcionusuario);
        //funcionusuario.setText(objUsuarioAutenticado.getUsu_funcion());
        funcionusuario.setText(objUsuarioInicioPrueba.getCargoEmpresa());

        TextView empresausuario = (TextView)header.findViewById(R.id.navheader_empresausuario);
        //empresausuario.setText(objUsuarioAutenticado.getUsu_nomempresa());
        empresausuario.setText(objUsuarioInicioPrueba.getNombreEmpresa());

        imgAvatarUsuario = (CircleImageView)header.findViewById(R.id.navheader_imgAvatarUsuario);

        //Obtenemos la imagen a mostrar, en caso el usuario no tenga imagen mostramos la imagen por defecto
        //String urlImagenUsuario = objUsuarioAutenticado.getUsu_avatar();
        String urlImagenUsuario = "";
        if(urlImagenUsuario.compareTo("")==0) urlImagenUsuario = "uploads/usuario/default.png";

        Glide.with(this)
                //.load("https://cdn2.iconfinder.com/data/icons/perfect-flat-icons-2/512/User_man_male_profile_account_person_people.png")
                .load(ConstantesUtils.Servidor+urlImagenUsuario)
                .centerCrop()
                .into(imgAvatarUsuario);

        if (navigationView != null) {
            prepararDrawer(navigationView);
            seleccionarItem(navigationView.getMenu().getItem(0));
        }
    }

    public Menu getMenu(){
        return navigationView.getMenu();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //Cambiamos la fuente del toolbar
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void prepararDrawer(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // Marcar item presionado
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawerLayout.closeDrawers();
                        return true;
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void seleccionarItem(MenuItem itemDrawer) {
        Fragment fragmentoGenerico = null;
        FragmentManager fragmentManager = getSupportFragmentManager();

        final SweetAlertDialog dialog =  new SweetAlertDialog(PrincipalActivity.this,SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        Handler handler = new Handler();
        String TituloToolbar = "";

        switch (itemDrawer.getItemId()) {
            case R.id.nav_home:
                fragmentoGenerico = new InicioPrincipalFragment();
                TituloToolbar = "Panel de Control";
                setTitle(TituloToolbar);
                break;
            case R.id.nav_usuarios:
                fragmentoGenerico = new UsuarioPrincipalFragment();
                TituloToolbar = "Usuarios";
                setTitle(TituloToolbar);
                break;
            case R.id.nav_clientes:
                fragmentoGenerico = new ClientePrincipalFragment();
                TituloToolbar = "Clientes";
                setTitle(TituloToolbar);
                break;
            case R.id.nav_proveedores:
                fragmentoGenerico = new ProveedorPrincipalFragment();
                TituloToolbar = "Proveedores";
                setTitle(TituloToolbar);
                break;
            case R.id.nav_productos:
                dialog.setTitleText("Productos");
                dialog.setContentText("Módulo en desarrollo");
                dialog.setCustomImage(R.drawable.icon_cart48x48);
                dialog.setCancelable(false);
                dialog.show();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //dialog.cancel();
                        dialog.dismiss();
                    }
                }, 2000);
                break;
            case R.id.nav_categorias:
                fragmentoGenerico = new CategoriaProductoPrincipalFragment();
                TituloToolbar = "Categorías de Productos";
                setTitle(TituloToolbar);
                break;
            case R.id.nav_ventas:
                dialog.setTitleText("Ventas");
                dialog.setContentText("Módulo en desarrollo");
                dialog.setCustomImage(R.drawable.icon_currency_usd48x48);
                dialog.setCancelable(false);
                dialog.show();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //dialog.cancel();
                        dialog.dismiss();
                    }
                }, 2000);
                break;
            case R.id.nav_reportes:
                dialog.setTitleText("Reportes");
                dialog.setContentText("Módulo en desarrollo");
                dialog.setCustomImage(R.drawable.icon_chart_bar48x48);
                dialog.setCancelable(false);
                dialog.show();
                //dialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //dialog.cancel();
                        dialog.dismiss();
                    }
                }, 2000);
                break;
            case R.id.nav_about:
                try
                {
                    final AlertDialog.Builder dialogAcercaDe;
                    final View vistaAcercaDe;
                    vistaAcercaDe = getLayoutInflater().inflate(R.layout.dialog_acercade, null);
                    WebView textoAcercaDe = (WebView)vistaAcercaDe.findViewById(R.id.lblTexto_AcercaDe);
                    String text;
                    text = "<html><body><p align=\"justify\">";
                    //text+= "SGA Móvil es un sistema ERP que te permitirá optimizar todas tus operaciones y procesos de control para la mejora continua por medio de herramientas innovadoras y de simplificación de datos.";
                    text += "Es un sistema de Planificación de Recursos Empresariales (ERP) desarrollado para Web y Android con sincronización online en la nube, usando herramientas innovadoras que simplifican y automatizan el ingreso de datos, potenciando la gestión de información para la correcta toma de decisiones de su negocio.";
                    text+= "</p></body></html>";
                    textoAcercaDe.loadData(text, "text/html; charset=utf-8", "UTF-8");

                    dialogAcercaDe =
                            new AlertDialog.Builder(PrincipalActivity.this)
                                    .setView(vistaAcercaDe);
                    TextView title = new TextView(this);
                    title.setText("SGA Móvil");
                    title.setPadding(10, 10, 10, 10);
                    title.setGravity(Gravity.CENTER);
                    title.setTextColor(Color.BLACK);
                    title.setTextSize(18);
                    dialogAcercaDe.setNegativeButton("OK", null);
                    AlertDialog alertDialog = dialogAcercaDe.create();
                    alertDialog.show();
                }
                catch (Exception ex)
                {
                    String error = ex.getMessage();
                    String abc = "20";
                }
                break;

            case R.id.nav_log_out:
                CerrarSesion();
                break;
        }
        if (fragmentoGenerico != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.contenedor_principal, fragmentoGenerico)
                    .commit();
        }
    }

    public void seleccionarFragment(Fragment fragment, boolean addbackstack)
    {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.contenedor_principal, fragment);
        if (addbackstack)
            transaction.addToBackStack("Gestión de Usuarios");
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        CerrarSesion();
    }

    private void CerrarSesion()
    {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Cerrar Sesión")
                .setContentText("¿Está seguro que desea salir de la aplicación?")
                .setConfirmText("Si")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        AutenticacionUtils.CerrarSesion(PrincipalActivity.this);
                        finish();
                    }
                })
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
