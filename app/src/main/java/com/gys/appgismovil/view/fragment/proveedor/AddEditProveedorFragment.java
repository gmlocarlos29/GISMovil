package com.gys.appgismovil.view.fragment.proveedor;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.Proveedor;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.FechaUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 06/09/2017.
 */
public class AddEditProveedorFragment extends Fragment
{
    private static final String ARG_PROVEEDOR_ID = "arg_proveedor_id";

    private String mProveedorId;

    private TextInputLayout tilRazonSocial;
    private EditText txtRazonSocial;
    private TextInputLayout tilNroDocumento;
    private EditText txtNroDocumento;
    private TextInputLayout tilTelefono;
    private EditText txtTelefono;
    private TextInputLayout tilEmail;
    private EditText txtEmail;
    private TextInputLayout tilDireccion;
    private EditText txtDireccion;
    private TextInputLayout tilCiudad;
    private EditText txtCiudad;
    private TextInputLayout tilContacto;
    private EditText txtContacto;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private FloatingActionButton fabGuardar;

    Usuario objUsuarioAutenticado;

    public AddEditProveedorFragment() {
        // Required empty public constructor
    }

    public static AddEditProveedorFragment newInstance(String lawyerId)
    {
        AddEditProveedorFragment fragment = new AddEditProveedorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PROVEEDOR_ID, lawyerId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mProveedorId = getArguments().getString(ARG_PROVEEDOR_ID);
        }
        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        //Obtenemos el usuario logueado
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_proveedor_addedit, container, false);
        IniciarControles(view);

        // Carga de datos
        if (mProveedorId != null)
        {
            LoadProveedor();
        }

        return view;
    }

    private void IniciarControles(View view)
    {
        tilRazonSocial = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_razonsocial);
        txtRazonSocial = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_razonsocial);
        tilNroDocumento = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_nrodoc);
        txtNroDocumento = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_nrodoc);
        tilTelefono = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_telefono);
        txtTelefono = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_telefono);
        tilEmail = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_correo);
        txtEmail = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_correo);
        tilDireccion = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_direccion);
        txtDireccion = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_direccion);
        tilCiudad = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_ciudad);
        txtCiudad = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_ciudad);
        tilContacto = (TextInputLayout)view.findViewById(R.id.til_proveedorAddEditFragment_contacto);
        txtContacto = (EditText)view.findViewById(R.id.txt_proveedorAddEditFragment_contacto);

        fabGuardar = (FloatingActionButton)getActivity().findViewById(R.id.proveedorAddEditActivity_fabGuardar);

        fabGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarProveedor();
            }
        });
    }

    private void LoadProveedor()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Proveedor> callObtenerProveedor = mRESTApi.ObtenerProveedorPorId(mProveedorId);
        callObtenerProveedor.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Proveedor objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosProveedor(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosProveedor(Proveedor proveedor)
    {
        txtRazonSocial.setText(proveedor.getProv_razonsocial());
        txtNroDocumento.setText(proveedor.getProv_docnro());
        txtTelefono.setText(proveedor.getProv_telefono());
        txtEmail.setText(proveedor.getProv_correo());
        txtDireccion.setText(proveedor.getProv_direccion());
        txtCiudad.setText(proveedor.getProv_ciudad());
        txtContacto.setText(proveedor.getProv_contacto());
    }

    private void GuardarProveedor()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            boolean error = false;

            //Obtenemos los datos del proveedor
            String razonsocial = txtRazonSocial.getText().toString();
            String nrodoc = txtNroDocumento.getText().toString();
            String telefono = txtTelefono.getText().toString();
            String email = txtEmail.getText().toString();
            String direccion = txtDireccion.getText().toString();
            String ciudad = txtCiudad.getText().toString();
            String contacto = txtContacto.getText().toString();
            int anio = FechaUtils.GetAnioActual();
            int idempresa = objUsuarioAutenticado.getUsu_idempresa();

            if (TextUtils.isEmpty(razonsocial)) {
                txtRazonSocial.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(nrodoc)) {
                txtNroDocumento.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(telefono)) {
                txtTelefono.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(email)) {
                txtEmail.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(direccion)) {
                txtDireccion.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(ciudad)) {
                txtCiudad.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(contacto)) {
                txtContacto.setError(getString(R.string.field_error));
                error = true;
            }
            if (error) {
                return;
            }

            Proveedor proveedor = new Proveedor(idempresa,nrodoc,razonsocial,telefono,email,direccion,ciudad,contacto,anio);

            if(mProveedorId!=null)
            {
                proveedor.setProv_id(mProveedorId);
                ActualizarProveedor(proveedor);
            }
            else
            {
                RegistrarProveedor(proveedor);
            }
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    private void ActualizarProveedor(Proveedor proveedor)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Proveedor> callActualizarProveedor = mRESTApi.ActualizarProveedor(proveedor);
        callActualizarProveedor.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }
                }

                fabGuardar.setEnabled(true);
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    private void RegistrarProveedor(Proveedor proveedor)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Proveedor> callRegistrarProveedor = mRESTApi.RegistrarProveedor(proveedor);
        callRegistrarProveedor.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }

                    fabGuardar.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

}
