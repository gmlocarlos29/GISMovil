package com.gys.appgismovil.view.activity.comun;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.InicioPrueba;
import com.gys.appgismovil.data.sqlite.Contrato;
import com.gys.appgismovil.enumeradores.ePlan;
import com.gys.appgismovil.util.Autenticacion;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by USER on 29/12/2017.
 */
public class InicioPruebaActivity extends AppCompatActivity
{
    EditText txtNombreApellidos;
    EditText txtDocumentoEmpresa;
    EditText txtNombreEmpresa;
    EditText txtCargoEmpresa;

    InicioPrueba objInicioPrueba = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicioprueba);
        IniciarControles();
    }

    private void IniciarControles()
    {
        txtNombreApellidos = (EditText)findViewById(R.id.activityInicioPrueba_txtNombreApellidos);
        txtDocumentoEmpresa = (EditText)findViewById(R.id.activityInicioPrueba_txtNroDocumentoEmpresa);
        txtNombreEmpresa = (EditText)findViewById(R.id.activityInicioPrueba_txtNombreEmpresa);
        txtCargoEmpresa = (EditText)findViewById(R.id.activityInicioPrueba_txtCargoEmpresa);
    }

    public void ComenzarPruebaApp(View view)
    {
        // Obtenemos los datos ingresados por el usuario
        String NombreApellidos = txtNombreApellidos.getText().toString();
        String NroDocumentoEmpresa = txtDocumentoEmpresa.getText().toString();
        String NombreEmpresa = txtNombreEmpresa.getText().toString();
        String CargoEmpresa = txtCargoEmpresa.getText().toString();

        // Validamos si los datos ingresados son correctos
        if(DatosCorrectosComenzarPrueba(NombreApellidos,NroDocumentoEmpresa,NombreEmpresa,CargoEmpresa))
        {
            objInicioPrueba = new InicioPrueba();
            objInicioPrueba.setNombreApellidos(NombreApellidos);
            objInicioPrueba.setNroDocumentoEmpresa(NroDocumentoEmpresa);
            objInicioPrueba.setNombreEmpresa(NombreEmpresa);
            objInicioPrueba.setCargoEmpresa(CargoEmpresa);

            //Insertamos en la BD la Empresa, la Tienda y el Usuario
            GuardarInicioPruebaBD(objInicioPrueba);
        }
    }

    public void GuardarInicioPruebaBD(InicioPrueba objInicioPrueba)
    {
        new TaskGuardarInicioPruebaBD().execute(objInicioPrueba);
    }

    public class TaskGuardarInicioPruebaBD extends AsyncTask<Object,Void, ContentProviderResult[]>
    {
        @Override
        protected ContentProviderResult[] doInBackground(Object... params)
        {
            //Boolean result = false;

            // Obtenemos los parametros
            InicioPrueba objInicioPrueba = (InicioPrueba)params[0];

            ContentResolver resolver = getContentResolver();

            //Lista de Operaciones
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            // Insertamos la Empresa
            ops.add(ContentProviderOperation.newInsert(
                    Contrato.ContratoEmpresa.URI_CONTENIDO)
                            .withValue(Contrato.ContratoEmpresa.EMP_DOCTIPO, "RUC")
                            .withValue(Contrato.ContratoEmpresa.EMP_DOCNRO, objInicioPrueba.getNroDocumentoEmpresa())
                            .withValue(Contrato.ContratoEmpresa.EMP_RAZONSOCIAL, objInicioPrueba.getNombreEmpresa())
                            .withValue(Contrato.ContratoEmpresa.EMP_TELEFONO, "")
                            .withValue(Contrato.ContratoEmpresa.EMP_CORREO, "")
                            .withValue(Contrato.ContratoEmpresa.EMP_DIRECCION, "")
                            .withValue(Contrato.ContratoEmpresa.EMP_CIUDAD, "")
                            .withValue(Contrato.ContratoEmpresa.EMP_ADMINISTRADOR, "")
                            .withValue(Contrato.ContratoEmpresa.EMP_PLANAPP, ePlan.FREE)
                            .withValue(Contrato.ContratoEmpresa.EMP_PLANREGISTRO,"")
                            .withValue(Contrato.ContratoEmpresa.EMP_PLANVENCIMIENTO,"")
                            .withValue(Contrato.ContratoEmpresa.EMP_MAXUSER,"")
                            .withValue(Contrato.ContratoEmpresa.EMP_CANUSER,"")
                            .withValue(Contrato.ContratoEmpresa.EMP_ESTADO,"1")
                            .build()
            );

            // Insertamos la Tienda
            ops.add(ContentProviderOperation.newInsert(
                            Contrato.ContratoTienda.URI_CONTENIDO)
                            .withValue(Contrato.ContratoTienda.TND_NOMBRE, "TIENDA 1")
                            .withValue(Contrato.ContratoTienda.TND_IDEMPRESA, "")
                            .withValueBackReference(Contrato.ContratoTienda.TND_IDEMPRESA, 0)
                            .withValue(Contrato.ContratoTienda.TND_ESTADO, "1")
                            .build()
            );

            // Insertamos el Usuario
            ops.add(ContentProviderOperation.newInsert(
                            Contrato.ContratoUsuario.URI_CONTENIDO)
                            .withValueBackReference(Contrato.ContratoUsuario.USU_IDEMPRESA, 0)
                            .withValue(Contrato.ContratoUsuario.USU_USERNAME, "")
                            .withValue(Contrato.ContratoUsuario.USU_PASSWORD, "")
                            .withValue(Contrato.ContratoUsuario.USU_NOMBRE, objInicioPrueba.getNombreApellidos())
                            .withValue(Contrato.ContratoUsuario.USU_FUNCION, objInicioPrueba.getCargoEmpresa())
                            .withValue(Contrato.ContratoUsuario.USU_CORREO, "")
                            .withValue(Contrato.ContratoUsuario.USU_AVATAR, "")
                            .withValue(Contrato.ContratoUsuario.USU_ESTADO, "1")
                            .build()
            );


            ContentProviderResult[] results = null;

            try
            {
                results = resolver.applyBatch(Contrato.AUTORIDAD, ops);
                //result = true;
            }
            catch (RemoteException e)
            {
                e.printStackTrace();
                //result = false;
            }
            catch (OperationApplicationException e)
            {
                e.printStackTrace();
            }

            return results;
        }

        @Override
        protected void onPostExecute(ContentProviderResult[] contentProviderResults)
        {
            if(contentProviderResults!=null)
            {
                // Obtenemos los Ids Insertados
                Uri uriEmpresa = contentProviderResults[0].uri;
                Uri uriTienda = contentProviderResults[1].uri;
                Uri uriUsuario = contentProviderResults[2].uri;
                String idEmpresa = Contrato.ContratoEmpresa.obtenerIdEmpresa(uriEmpresa);
                String idTienda = Contrato.ContratoTienda.obtenerIdTienda(uriTienda);
                String idUsuario = Contrato.ContratoUsuario.obtenerIdUsuario(uriUsuario);
                objInicioPrueba.setIdEmpresa(Integer.parseInt(idEmpresa));
                objInicioPrueba.setIdTienda(Integer.parseInt(idTienda));
                objInicioPrueba.setIdUsuario(Integer.parseInt(idUsuario));

                // Registramos en SharedPreferences los datos del usuario para el inicio de pruebas
                Autenticacion.setUsuarioInicioPrueba(InicioPruebaActivity.this,objInicioPrueba);

                // Mostramos el Activity Principal
                Intent myIntent = new Intent(getApplicationContext(), PrincipalActivity.class);
                startActivity(myIntent);
            }
        }
    }

    public boolean DatosCorrectosComenzarPrueba(String NombreApellidos, String NroDocumentoEmpresa, String NombreEmpresa, String CargoEmpresa)
    {
        boolean resultado = true;
        final SweetAlertDialog pDialog = new SweetAlertDialog(InicioPruebaActivity.this, SweetAlertDialog.WARNING_TYPE);
        pDialog.setCancelable(false);

        if(NombreApellidos.isEmpty())
        {
            pDialog.setTitleText("Datos Incorrectos");
            pDialog.setContentText("Ingrese su Nombre y Apellidos");
            pDialog.show();
            resultado = false;
        }
        else if(NroDocumentoEmpresa.isEmpty())
        {
            pDialog.setTitleText("Datos Incorrectos");
            pDialog.setContentText("Ingrese el Número de Documento de su Empresa. Si no tiene una empresa ingrese su número de documento personal.");
            pDialog.show();
            resultado = false;
        }
        else if(NombreEmpresa.isEmpty())
        {
            pDialog.setTitleText("Datos Incorrectos");
            pDialog.setContentText("Ingrese el Nombre de su Empresa. Si no tiene una empresa ingrese cualquier nombre.");
            pDialog.show();
            resultado = false;
        }
        else if(CargoEmpresa.isEmpty())
        {
            pDialog.setTitleText("Datos Incorrectos");
            pDialog.setContentText("Ingrese su Cargo dentro de su Empresa. Si no tiene un cargo ingrese cualquier nombre.");
            pDialog.show();
            resultado = false;
        }

        return resultado;
    }
}
