package com.gys.appgismovil.view.activity.categoriaproducto;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.categoriaproducto.AddEditCategoriaProductoFragment;

/**
 * Created by USER on 16/10/2017.
 */
public class AddEditCategoriaProductoActivity extends AppCompatActivity
{
    public static final int REQUEST_ADD_CATEGORIAPRODUCTO = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoriaproducto_addedit);
        setToolbar();

        String categoriaproductoId = getIntent().getStringExtra(PrincipalActivity.EXTRA_CATEGORIAPRODUCTO_ID);
        setTitle(categoriaproductoId == null ? "Nueva Categoría de Producto" : "Editar Categoría de Producto");

        AddEditCategoriaProductoFragment fragment = (AddEditCategoriaProductoFragment)
                getSupportFragmentManager().findFragmentById(R.id.categoriaproductoAddEditActivity_container);
        if (fragment == null) {
            fragment = AddEditCategoriaProductoFragment.newInstance(categoriaproductoId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.categoriaproductoAddEditActivity_container, fragment)
                    .commit();
        }
    }

    public void setToolbar()
    {
        Toolbar toolbar = (Toolbar)findViewById(R.id.categoriaproductoAddEditActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
