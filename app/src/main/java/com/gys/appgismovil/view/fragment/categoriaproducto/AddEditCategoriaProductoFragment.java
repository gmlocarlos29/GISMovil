package com.gys.appgismovil.view.fragment.categoriaproducto;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.CategoriaProducto;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.FechaUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 16/10/2017.
 */
public class AddEditCategoriaProductoFragment extends Fragment
{
    private static final String ARG_CATEGORIAPRODUCTO_ID = "arg_categoriaproducto_id";

    private String mCategoriaProductoId;
    private TextInputLayout tilNombre;
    private EditText txtNombre;
    private TextInputLayout tilDescripcion;
    private EditText txtDescripcion;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private FloatingActionButton fabGuardar;

    Usuario objUsuarioAutenticado;

    public AddEditCategoriaProductoFragment() {
        // Required empty public constructor
    }

    public static AddEditCategoriaProductoFragment newInstance(String categoriaproductoId) {
        AddEditCategoriaProductoFragment fragment = new AddEditCategoriaProductoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORIAPRODUCTO_ID, categoriaproductoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mCategoriaProductoId = getArguments().getString(ARG_CATEGORIAPRODUCTO_ID);
        }
        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        //Obtenemos el usuario logueado
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_categoriaproducto_addedit, container, false);
        IniciarControles(view);

        // Carga de datos
        if (mCategoriaProductoId != null)
        {
            LoadCategoriaProducto();
        }

        return view;
    }

    private void LoadCategoriaProducto()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CategoriaProducto> callObtenerCategoria = mRESTApi.ObtenerCategoriaProductoPorId(mCategoriaProductoId);
        callObtenerCategoria.enqueue(new Callback<CategoriaProducto>() {
            @Override
            public void onResponse(Call<CategoriaProducto> call, Response<CategoriaProducto> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    CategoriaProducto objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosCategoriaProducto(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriaProducto> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosCategoriaProducto(CategoriaProducto categoriaProducto)
    {
        txtNombre.setText(categoriaProducto.getCatpro_nombre());
        txtDescripcion.setText(categoriaProducto.getCatpro_descripcion());
    }

    private void IniciarControles(View view)
    {
        tilNombre = (TextInputLayout)view.findViewById(R.id.til_categoriaproductoAddEditFragment_nombre);
        txtNombre = (EditText)view.findViewById(R.id.txt_categoriaproductoAddEditFragment_nombre);
        tilDescripcion = (TextInputLayout)view.findViewById(R.id.til_categoriaproductoAddEditFragment_descripcion);
        txtDescripcion = (EditText)view.findViewById(R.id.txt_categoriaproductoAddEditFragment_descripcion);

        fabGuardar = (FloatingActionButton)getActivity().findViewById(R.id.categoriaproductoAddEditActivity_fabGuardar);

        fabGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarCategoriaProducto();
            }
        });
    }

    private void GuardarCategoriaProducto()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            boolean error = false;

            //Obtenemos los datos de la categoria
            String nombre = txtNombre.getText().toString();
            String descripcion = txtDescripcion.getText().toString();
            int idempresa = objUsuarioAutenticado.getUsu_idempresa();

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(descripcion)) {
                txtDescripcion.setError(getString(R.string.field_error));
                error = true;
            }
            if (error) {
                return;
            }

            CategoriaProducto categoriaProducto = new CategoriaProducto(nombre,idempresa,descripcion);

            if(mCategoriaProductoId!=null)
            {
                categoriaProducto.setCatpro_id((Integer.parseInt(mCategoriaProductoId)));
                ActualizarCategoriaProducto(categoriaProducto);
            }
            else
            {
                RegistrarCategoriaProducto(categoriaProducto);
            }
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    private void RegistrarCategoriaProducto(CategoriaProducto categoriaProducto)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CategoriaProducto> callRegistrarCategoria = mRESTApi.RegistrarCategoriaProducto(categoriaProducto);
        callRegistrarCategoria.enqueue(new Callback<CategoriaProducto>() {
            @Override
            public void onResponse(Call<CategoriaProducto> call, Response<CategoriaProducto> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }

                    fabGuardar.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<CategoriaProducto> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    private void ActualizarCategoriaProducto(CategoriaProducto categoriaProducto)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CategoriaProducto> callActualizarCategoria = mRESTApi.ActualizarCategoriaProducto(categoriaProducto);
        callActualizarCategoria.enqueue(new Callback<CategoriaProducto>() {
            @Override
            public void onResponse(Call<CategoriaProducto> call, Response<CategoriaProducto> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }
                }

                fabGuardar.setEnabled(true);
            }

            @Override
            public void onFailure(Call<CategoriaProducto> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

}
