package com.gys.appgismovil.view.activity.proveedor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.proveedor.AddEditProveedorFragment;

/**
 * Created by USER on 06/09/2017.
 */
public class AddEditProveedorActivity extends AppCompatActivity
{
    public static final int REQUEST_ADD_PROVEEDOR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveedor_addedit);
        setToolbar();

        String proveedorId = getIntent().getStringExtra(PrincipalActivity.EXTRA_PROVEEDOR_ID);
        setTitle(proveedorId == null ? "Nuevo Proveedor" : "Editar Proveedor");

        AddEditProveedorFragment fragment = (AddEditProveedorFragment)
                getSupportFragmentManager().findFragmentById(R.id.proveedorAddEditActivity_container);
        if (fragment == null) {
            fragment = AddEditProveedorFragment.newInstance(proveedorId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.proveedorAddEditActivity_container, fragment)
                    .commit();
        }
    }

    public void setToolbar()
    {
        Toolbar toolbar = (Toolbar)findViewById(R.id.proveedorAddEditActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
