package com.gys.appgismovil.view.fragment.usuario;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.comun.ListaUsuario;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.activity.usuario.AddEditUsuarioActivity;
import com.gys.appgismovil.view.activity.usuario.UsuarioDetalleActivity;
import com.gys.appgismovil.view.adapter.ListaUsuariosAdapter;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 24/08/2017.
 */
public class UsuarioPrincipalFragment extends Fragment implements ListaUsuariosAdapter.OnItemClickListener
{
    private RecyclerView recycler;
    private ListaUsuariosAdapter adapter;
    private RecyclerView.LayoutManager lManager;
    private ListaUsuario listaUsuario;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    SwipeRefreshLayout srlListaUsuarios;

    private FloatingActionButton fabNuevoUsuario;

    public static final int REQUEST_UPDATE_DELETE_USUARIO = 2;

    private Usuario objUsuarioAutenticado;

    public UsuarioPrincipalFragment() {
        // Required empty public constructor
    }

    public void IniciarControles(View view){

        // Obtener el Recycler
        recycler = (RecyclerView) view.findViewById(R.id.fragmentUsuariosPrincipal_rcvUsuarios);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        listaUsuario = new ListaUsuario();
        adapter = new ListaUsuariosAdapter(getActivity(),new ArrayList<Usuario>(),this);
        recycler.setAdapter(adapter);

        //Obtenemos el refresh layout
        srlListaUsuarios = (SwipeRefreshLayout)view.findViewById(R.id.srlListaUsuarios);

        // Seteamos los colores que se usarán a lo largo de la animación
        srlListaUsuarios.setColorSchemeResources(
                R.color.s1,
                R.color.s2,
                R.color.s3,
                R.color.s4
        );
        // Iniciar la tarea asíncrona al revelar el indicador
        srlListaUsuarios.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ObtenerListaUsuariosRefresh();
                    }
                }
        );

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        ObtenerListaUsuarios();

        fabNuevoUsuario = (FloatingActionButton)view.findViewById(R.id.cmnFragmentoUsuariosPrincipal_fabNuevoUsuario);
        fabNuevoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarActivityRegistrarUsuario();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_usuarios_principal, container, false);
        IniciarControles(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void ObtenerListaUsuariosRefresh()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            Call<ListaUsuario> callListarUsuarios = mRESTApi.ListarUsuariosPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarUsuarios.enqueue(new Callback<ListaUsuario>() {
                @Override
                public void onResponse(Call<ListaUsuario> call, Response<ListaUsuario> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaUsuario = response.body();

                        if(listaUsuario.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaUsuariosAdapter(getActivity(),listaUsuario.Detalle,UsuarioPrincipalFragment.this);
                            recycler.setAdapter(adapter);

                            srlListaUsuarios.setRefreshing(false);
                        }
                        else if(listaUsuario.Mensaje.Status == eStatus.Error)
                        {
                            srlListaUsuarios.setRefreshing(false);

                            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaUsuario.Mensaje.DetalleMensaje);
                            pDialog.show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaUsuario> call, Throwable t) {
                    srlListaUsuarios.setRefreshing(false);

                    final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                    pDialog.show();
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    public void ObtenerListaUsuarios()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Cargando Datos...");
            pDialog.setCancelable(false);
            pDialog.show();

            Call<ListaUsuario> callListarUsuarios = mRESTApi.ListarUsuariosPorEmpresa(1);
            callListarUsuarios.enqueue(new Callback<ListaUsuario>() {
                @Override
                public void onResponse(Call<ListaUsuario> call, Response<ListaUsuario> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaUsuario = response.body();

                        if(listaUsuario.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaUsuariosAdapter(getActivity(),listaUsuario.Detalle,UsuarioPrincipalFragment.this);
                            recycler.setAdapter(adapter);
                            pDialog.dismiss();
                        }
                        else if(listaUsuario.Mensaje.Status == eStatus.Error)
                        {
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaUsuario.Mensaje.DetalleMensaje);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaUsuario> call, Throwable t) {
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    @Override
    public void onClick(ListaUsuariosAdapter.ListaUsuarioViewHolder holder, String IdUsuario) {
        MostrarActivityUsuarioDetalle(IdUsuario);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_UPDATE_DELETE_USUARIO:
                     ObtenerListaUsuarios();
                     break;
                case AddEditUsuarioActivity.REQUEST_ADD_USUARIO:
                    ObtenerListaUsuarios();
                    break;
            }
        }
    }

    public void MostrarActivityUsuarioDetalle(String IdUsuario)
    {
        Intent intent = new Intent(getActivity(), UsuarioDetalleActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_USUARIO_ID, IdUsuario);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_USUARIO);
    }

    public void MostrarActivityRegistrarUsuario()
    {
        Intent intent = new Intent(getActivity(), AddEditUsuarioActivity.class);
        startActivityForResult(intent, AddEditUsuarioActivity.REQUEST_ADD_USUARIO);
    }

}
