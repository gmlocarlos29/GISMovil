package com.gys.appgismovil.view.activity.cliente;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.cliente.ClienteDetalleFragment;

/**
 * Created by USER on 05/09/2017.
 */
public class ClienteDetalleActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_detalle);
        setToolbar();
        String id = getIntent().getStringExtra(PrincipalActivity.EXTRA_CLIENTE_ID);
        ClienteDetalleFragment fragment = (ClienteDetalleFragment)
                getSupportFragmentManager().findFragmentById(R.id.clienteDetalleActivity_container);
        if (fragment == null) {
            fragment = ClienteDetalleFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.clienteDetalleActivity_container, fragment)
                    .commit();
        }
    }

    private void setToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.clienteDetalleActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cliente_detalle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
