package com.gys.appgismovil.view.activity.comun;

import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gys.appgismovil.R;
import com.gys.appgismovil.view.utils.ConstantesUtils;

/**
 * Created by USER on 03/09/2017.
 */
public class ImagenActivity extends AppCompatActivity{

    ImageView imgImagen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
        CreateToolbar();
        IniciarControles();
        Uri uriImagen = Uri.parse(getIntent().getStringExtra("imageUri"));

        Glide.with(this)
                .load(uriImagen)
                .centerCrop()
                .into(imgImagen);
        //imgImagen.setImageURI(uriImagen);
    }

    public void CreateToolbar()
    {
        Toolbar toolbar = (Toolbar)findViewById(R.id.imagenActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void IniciarControles()
    {
        imgImagen = (ImageView)findViewById(R.id.img_imagenactivity_imagen);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
