package com.gys.appgismovil.view.utils;

import com.gys.appgismovil.data.model.comun.ListaUsuario;
import com.gys.appgismovil.data.model.comun.LoginBody;
import com.gys.appgismovil.data.model.comun.Mensaje;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.CategoriaProducto;
import com.gys.appgismovil.data.model.entidades.Cliente;
import com.gys.appgismovil.data.model.entidades.ListaCategoriaProducto;
import com.gys.appgismovil.data.model.entidades.ListaCliente;
import com.gys.appgismovil.data.model.entidades.ListaProveedor;
import com.gys.appgismovil.data.model.entidades.Proveedor;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by USER on 23/08/2017.
 */
public interface RESTApiUtils
{
    public static final String BASE_URL = "http://192.168.0.13:8080/GISMovil/public/"; // Red wifi casa
    //public static final String BASE_URL = "http://10.0.3.2:8080/GISMovil/public/"; // Genymotion

    //Usuarios
    @POST("Usuarios/Autenticar")Call<Usuario> Autenticar(@Body LoginBody loginBody);
    @GET("Usuarios/ListarPorEmpresa/{idEmpresa}")Call<ListaUsuario> ListarUsuariosPorEmpresa(@Path("idEmpresa") int idEmpresa);
    @GET("Usuarios/ObtenerPorId/{idUsuario}")Call<Usuario> ObtenerUsuarioPorId(@Path("idUsuario") String idUsuario);
    @PUT("Usuarios/Actualizar")Call<Usuario> ActualizarUsuario(@Body Usuario usuario);
    @Multipart
    @POST("Usuarios/Actualizar2")Call<Usuario> ActualizarUsuario2(@Part MultipartBody.Part imagen, @Part("usuario")Usuario usuario);
    @GET("Usuarios/Eliminar/{idUsuario}")Call<Usuario> EliminarUsuario(@Path("idUsuario") String idUsuario);
    @POST("Usuarios/Registrar")Call<Usuario> RegistrarUsuario(@Body Usuario usuario);
    @Multipart
    @POST("Usuarios/Registrar2")Call<Usuario> RegistrarUsuario2(@Part MultipartBody.Part imagen, @Part("usuario")Usuario usuario);

    //Clientes
    @GET("Clientes/ListarPorEmpresa/{idEmpresa}")Call<ListaCliente> ListarClientesPorEmpresa(@Path("idEmpresa") int idEmpresa);
    @POST("Clientes/Registrar")Call<Cliente> RegistrarCliente(@Body Cliente cliente);
    @GET("Clientes/ObtenerPorId/{idCliente}")Call<Cliente> ObtenerClientePorId(@Path("idCliente") String idCliente);
    @PUT("Clientes/Actualizar")Call<Cliente> ActualizarCliente(@Body Cliente cliente);
    @GET("Clientes/Eliminar/{idCliente}")Call<Cliente> EliminarCliente(@Path("idCliente") String idCliente);

    //Proveedores
    @GET("Proveedores/ListarPorEmpresa/{idEmpresa}")Call<ListaProveedor> ListarProveedoresPorEmpresa(@Path("idEmpresa") int idEmpresa);
    @POST("Proveedores/Registrar")Call<Proveedor> RegistrarProveedor(@Body Proveedor proveedor);
    @GET("Proveedores/ObtenerPorId/{idProveedor}")Call<Proveedor> ObtenerProveedorPorId(@Path("idProveedor") String idProveedor);
    @PUT("Proveedores/Actualizar")Call<Proveedor> ActualizarProveedor(@Body Proveedor proveedor);
    @GET("Proveedores/Eliminar/{idProveedor}")Call<Proveedor> EliminarProveedor(@Path("idProveedor") String idProveedor);

    //Categorias de Productos
    @GET("CategoriaProducto/ListarPorEmpresa/{idEmpresa}")
    Call<ListaCategoriaProducto> ListarCategoriaProductoPorEmpresa(@Path("idEmpresa") int idEmpresa);
    @POST("CategoriaProducto/Registrar")
    Call<CategoriaProducto> RegistrarCategoriaProducto(@Body CategoriaProducto categoriaProducto);
    @GET("CategoriaProducto/ObtenerPorId/{idCategoriaProducto}")
    Call<CategoriaProducto> ObtenerCategoriaProductoPorId(@Path("idCategoriaProducto") String idCategoriaProducto);
    @PUT("CategoriaProducto/Actualizar")
    Call<CategoriaProducto> ActualizarCategoriaProducto(@Body CategoriaProducto categoriaProducto);
    @GET("CategoriaProducto/Eliminar/{idCategoriaProducto}")
    Call<CategoriaProducto> EliminarCategoriaProducto(@Path("idCategoriaProducto") String idCategoriaProducto);

    @Multipart
    @POST("Main/SubirImagen")Call<Mensaje> SubitImagen (@Part MultipartBody.Part imagen);

}

