package com.gys.appgismovil.view.activity.usuario;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.usuario.AddEditUsuarioFragment;

/**
 * Created by USER on 28/08/2017.
 */
public class AddEditUsuarioActivity extends AppCompatActivity
{
    public static final int REQUEST_ADD_USUARIO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_addedit);
        Toolbar toolbar = (Toolbar)findViewById(R.id.usuarioAddEditActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String usuarioId = getIntent().getStringExtra(PrincipalActivity.EXTRA_USUARIO_ID);
        setTitle(usuarioId == null ? "Nuevo Usuario" : "Editar Usuario");

        AddEditUsuarioFragment addEditUsuarioFragment = (AddEditUsuarioFragment)
                getSupportFragmentManager().findFragmentById(R.id.usuarioAddEditActivity_container);
        if (addEditUsuarioFragment == null) {
            addEditUsuarioFragment = AddEditUsuarioFragment.newInstance(usuarioId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.usuarioAddEditActivity_container, addEditUsuarioFragment)
                    .commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
    */
}
