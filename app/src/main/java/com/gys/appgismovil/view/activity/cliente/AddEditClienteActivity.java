package com.gys.appgismovil.view.activity.cliente;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.cliente.AddEditClienteFragment;

/**
 * Created by USER on 04/09/2017.
 */
public class AddEditClienteActivity extends AppCompatActivity
{
    public static final int REQUEST_ADD_CLIENTE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente_addedit);
        setToolbar();

        String clienteId = getIntent().getStringExtra(PrincipalActivity.EXTRA_CLIENTE_ID);
        setTitle(clienteId == null ? "Nuevo Cliente" : "Editar Cliente");

        AddEditClienteFragment fragment = (AddEditClienteFragment)
                getSupportFragmentManager().findFragmentById(R.id.clienteAddEditActivity_container);
        if (fragment == null) {
            fragment = AddEditClienteFragment.newInstance(clienteId);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.clienteAddEditActivity_container, fragment)
                    .commit();
        }
    }

    public void setToolbar()
    {
        Toolbar toolbar = (Toolbar)findViewById(R.id.clienteAddEditActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
