package com.gys.appgismovil.view.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.gys.appgismovil.data.model.comun.Usuario;

/**
 * Created by USER on 23/08/2017.
 */
public class AutenticacionUtils
{
    public static int setUsuarioRecordado(Context contexto, Usuario usuario)
    {
        SharedPreferences pref = contexto.getSharedPreferences("UsuarioRecordado", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("usu_docempresa", usuario.getUsu_docempresa());
        editor.putString("usu_username", usuario.getUsu_username());
        editor.putString("usu_password", usuario.getUsu_password());
        editor.commit(); // commit changes
        return 1;
    }

    public static Usuario getUsuarioRecordado(Context contexto)
    {
        Usuario objUsuario = null;

        SharedPreferences pref = contexto.getSharedPreferences("UsuarioRecordado",0); // 0 - for private mode

        if(pref.contains("usu_docempresa"))
        {
            objUsuario = new Usuario();
            objUsuario.setUsu_docempresa(pref.getString("usu_docempresa",""));
            objUsuario.setUsu_username(pref.getString("usu_username",""));
            objUsuario.setUsu_password(pref.getString("usu_password",""));
        }
        return objUsuario;
    }

    public static void BorrarUsuarioRecordado(Context contexto)
    {
        SharedPreferences pref = contexto.getSharedPreferences("UsuarioRecordado",0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public static Usuario getUsuarioAutenticado(Context contexto)
    {
        Usuario objUsuario = null;

        SharedPreferences pref = contexto.getSharedPreferences("UsuarioAutenticado",0); // 0 - for private mode

        if(pref.contains("usu_id"))
        {
            objUsuario = new Usuario();
            objUsuario.setUsu_id(pref.getString("usu_id", ""));
            objUsuario.setUsu_idempresa(pref.getInt("usu_idempresa",0));
            objUsuario.setUsu_docempresa(pref.getString("usu_docempresa",""));
            objUsuario.setUsu_nomempresa(pref.getString("usu_nomempresa",""));
            objUsuario.setUsu_username(pref.getString("usu_username",""));
            objUsuario.setUsu_password(pref.getString("usu_password",""));
            objUsuario.setUsu_nombre(pref.getString("usu_nombre",""));
            objUsuario.setUsu_funcion(pref.getString("usu_funcion",""));
            objUsuario.setUsu_avatar(pref.getString("usu_avatar",""));
            objUsuario.setUsu_estado(pref.getString("usu_estado",""));
        }
        return objUsuario;
    }

    public static int setUsuarioAutenticado(Context contexto, Usuario usuario)
    {
        SharedPreferences pref = contexto.getSharedPreferences("UsuarioAutenticado", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("usu_id", usuario.getUsu_id());
        editor.putInt("usu_idempresa", usuario.getUsu_idempresa());
        editor.putString("usu_docempresa", usuario.getUsu_docempresa());
        editor.putString("usu_nomempresa", usuario.getUsu_nomempresa());
        editor.putString("usu_username", usuario.getUsu_username());
        editor.putString("usu_password", usuario.getUsu_password());
        editor.putString("usu_nombre", usuario.getUsu_nombre());
        editor.putString("usu_funcion", usuario.getUsu_funcion());
        editor.putString("usu_correo", usuario.getUsu_correo());
        editor.putString("usu_avatar", usuario.getUsu_avatar());
        editor.putString("usu_estado", usuario.getUsu_estado());
        editor.commit(); // commit changes
        return 1;
    }

    public static void CerrarSesion(Context contexto)
    {
        SharedPreferences pref = contexto.getSharedPreferences("UsuarioAutenticado",0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
