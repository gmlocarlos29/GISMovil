package com.gys.appgismovil.view.fragment.categoriaproducto;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.CategoriaProducto;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.categoriaproducto.AddEditCategoriaProductoActivity;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 16/10/2017.
 */
public class CategoriaProductoDetalleFragment extends Fragment
{
    private static final String ARG_CATEGORIAPRODUCTO_ID = "CategoriaProductoId";
    private String mCategoriaProductoId;

    private TextView tvNombre;
    private TextView tvDescripcion;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    public CategoriaProductoDetalleFragment() {
        // Required empty public constructor
    }

    public static CategoriaProductoDetalleFragment newInstance(String categoriaproductoId)
    {
        CategoriaProductoDetalleFragment fragment = new CategoriaProductoDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORIAPRODUCTO_ID, categoriaproductoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
            mCategoriaProductoId = getArguments().getString(ARG_CATEGORIAPRODUCTO_ID);

        setHasOptionsMenu(true);

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View  view = inflater.inflate(R.layout.fgt_categoriaproducto_detalle, container, false);
        tvNombre = (TextView)view.findViewById(R.id.tv_categoriaproductodetalle_nombre);
        tvDescripcion = (TextView)view.findViewById(R.id.tv_categoriaproductodetalle_descripcion);
        LoadCategoriaProducto();
        return view;
    }

    private void LoadCategoriaProducto()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CategoriaProducto> callObtenerCategoria = mRESTApi.ObtenerCategoriaProductoPorId(mCategoriaProductoId);
        callObtenerCategoria.enqueue(new Callback<CategoriaProducto>() {
            @Override
            public void onResponse(Call<CategoriaProducto> call, Response<CategoriaProducto> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    CategoriaProducto objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosCategoriaProducto(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriaProducto> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosCategoriaProducto(CategoriaProducto categoriaProducto)
    {
        tvNombre.setText(categoriaProducto.getCatpro_nombre());
        tvDescripcion.setText(categoriaProducto.getCatpro_descripcion());
    }

    public void LoadEditarCategoriaProductoActivity()
    {
        Intent intent = new Intent(getActivity(), AddEditCategoriaProductoActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_CATEGORIAPRODUCTO_ID, mCategoriaProductoId);
        startActivityForResult(intent, CategoriaProductoPrincipalFragment.REQUEST_UPDATE_DELETE_CATEGORIAPRODUCTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CategoriaProductoPrincipalFragment.REQUEST_UPDATE_DELETE_CATEGORIAPRODUCTO)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_edit:
                LoadEditarCategoriaProductoActivity();
                break;
            case R.id.action_delete:
                ConfirmarEliminacionCategoriaProducto();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void ConfirmarEliminacionCategoriaProducto()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Confirmar eliminación");
        pDialog.setContentText("¿Está seguro que desea eliminar la categoría?");
        pDialog.setConfirmText("Sí");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                EliminarCategoriaProducto();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    private void EliminarCategoriaProducto()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Eliminando Categoría...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CategoriaProducto> callEliminarCategoria = mRESTApi.EliminarCategoriaProducto(mCategoriaProductoId);
        callEliminarCategoria.enqueue(new Callback<CategoriaProducto>() {
            @Override
            public void onResponse(Call<CategoriaProducto> call, Response<CategoriaProducto> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    CategoriaProducto objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriaProducto> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());
            }
        });
    }

}
