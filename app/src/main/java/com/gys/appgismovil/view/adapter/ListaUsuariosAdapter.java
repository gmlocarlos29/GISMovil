package com.gys.appgismovil.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.view.utils.ConstantesUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by USER on 24/08/2017.
 */
public class ListaUsuariosAdapter extends RecyclerView.Adapter<ListaUsuariosAdapter.ListaUsuarioViewHolder>
{
    private List<Usuario> items;
    Context context;

    private OnItemClickListener escucha;

    public interface OnItemClickListener
    {
        public void onClick(ListaUsuarioViewHolder holder, String IdUsuario);
    }

    @Override
    public void onBindViewHolder(ListaUsuarioViewHolder holder, int position) {
        holder.lblNombre.setText(items.get(position).getUsu_nombre());
        holder.lblFuncion.setText(items.get(position).getUsu_funcion());
        final ImageView imgUsuario = holder.imgUsuario;
        //Obtenemos la imagen a mostrar, en caso el usuario no tenga imagen mostramos la imagen por defecto
        String urlImagenUsuario = items.get(position).getUsu_avatar();
        if(urlImagenUsuario.compareTo("")==0) urlImagenUsuario = "uploads/usuario/default.png";

        Glide
                .with(context)
                //.load("https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAuOAAAAJDM1YTMzMTNmLTBmM2MtNGFhNi1hZTVlLTc0MjIzNDdmODI4YQ.jpg")
                .load(ConstantesUtils.Servidor+urlImagenUsuario)
                //.load("http://192.168.0.11:8080/uploads/usuario/1_20170903_020812.jpg")
                .asBitmap()
                .error(R.drawable.ic_account_circle)
                .centerCrop()
                .into(new BitmapImageViewTarget(imgUsuario) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable drawable
                                = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        drawable.setCircular(true);
                        imgUsuario.setImageDrawable(drawable);
                    }
                });
    }

    public ListaUsuariosAdapter(Context context,List<Usuario> items, OnItemClickListener escucha)
    {
        this.items = items;
        this.context = context;
        this.escucha = escucha;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ListaUsuarioViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragmentusuariosprincipal_rcvusuarios, parent, false);
        return new ListaUsuarioViewHolder(v,this);
    }

    public class ListaUsuarioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //Campos respectivos de un item
        ImageView imgUsuario;
        TextView lblNombre;
        TextView lblFuncion;

        public ListaUsuarioViewHolder(View view,ListaUsuariosAdapter adapter)
        {
            super(view);
            imgUsuario = (ImageView)view.findViewById(R.id.itemFragmentUsuariosPrincipal_imgUsuario);
            lblNombre = (TextView)view.findViewById(R.id.itemFragmentUsuarioprincipal_lblNombre);
            lblFuncion = (TextView)view.findViewById(R.id.itemFragmentUsuariosPrincipal_lblFuncion);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            escucha.onClick(this,ObtenerIdUsuario(getAdapterPosition()));
        }
    }

    /*
    private String ObtenerIdUsuario(int posicion)
    {
        if(items!=null)
        {
            if(items != null)
            {
                return items.get(posicion).getUsu_id();
            }
            else
            {
                return "";
            }
        }
        else
        {
            return "";
        }
    }
    */
    private String ObtenerIdUsuario(int posicion)
    {
        if(items!=null)
        {
            return items.get(posicion).getUsu_id();
        }
        else
        {
            return "";
        }
    }
}
