package com.gys.appgismovil.view.fragment.proveedor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.ListaProveedor;
import com.gys.appgismovil.data.model.entidades.Proveedor;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.activity.proveedor.AddEditProveedorActivity;
import com.gys.appgismovil.view.activity.proveedor.ProveedorDetalleActivity;
import com.gys.appgismovil.view.adapter.ListaClientesAdapter;
import com.gys.appgismovil.view.adapter.ListaProveedorAdapter;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 06/09/2017.
 */
public class ProveedorPrincipalFragment extends Fragment implements ListaProveedorAdapter.OnItemClickListener
{
    private RecyclerView recycler;
    private ListaProveedorAdapter adapter;
    private RecyclerView.LayoutManager lManager;
    private ListaProveedor listaProveedor;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private SwipeRefreshLayout srlListaProveedores;

    private FloatingActionButton fabNuevoProveedor;

    public static final int REQUEST_UPDATE_DELETE_PROVEEDOR = 2;

    private Usuario objUsuarioAutenticado;

    public ProveedorPrincipalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_proveedores_principal, container, false);
        IniciarControles(view);
        return view;
    }

    private void IniciarControles(View view)
    {
        // Obtener el Recycler
        recycler = (RecyclerView) view.findViewById(R.id.fragmentProveedoresPrincipal_rcvProveedores);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        listaProveedor = new ListaProveedor();
        adapter = new ListaProveedorAdapter(getActivity(),new ArrayList<Proveedor>(),this);
        recycler.setAdapter(adapter);

        //Obtenemos el refresh layout
        srlListaProveedores = (SwipeRefreshLayout)view.findViewById(R.id.srlListaProveedores);

        // Seteamos los colores que se usarán a lo largo de la animación
        srlListaProveedores.setColorSchemeResources(
                R.color.s1,
                R.color.s2,
                R.color.s3,
                R.color.s4
        );

        // Iniciar la tarea asíncrona al revelar el indicador
        srlListaProveedores.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ObtenerListaProveedoresRefresh();
                    }
                }
        );

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        ObtenerListaProveedores();

        fabNuevoProveedor = (FloatingActionButton)view.findViewById(R.id.fragmentProveedoresPrincipal_fabNuevoProveedor);
        fabNuevoProveedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarActivityRegistrarProveedor();
            }
        });
    }

    @Override
    public void onClick(ListaProveedorAdapter.ListaProveedorViewHolder holder, String IdProveedor) {
        MostrarActivityProveedorDetalle(IdProveedor);
    }


    private void MostrarActivityRegistrarProveedor()
    {
        Intent intent = new Intent(getActivity(), AddEditProveedorActivity.class);
        startActivityForResult(intent, AddEditProveedorActivity.REQUEST_ADD_PROVEEDOR);
    }

    private void MostrarActivityProveedorDetalle(String IdProveedor)
    {
        Intent intent = new Intent(getActivity(), ProveedorDetalleActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_PROVEEDOR_ID, IdProveedor);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_PROVEEDOR);
    }

    private void ObtenerListaProveedoresRefresh()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Cargando Datos...");
            pDialog.setCancelable(false);
            pDialog.show();

            Call<ListaProveedor> callListarProveedores = mRESTApi.ListarProveedoresPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarProveedores.enqueue(new Callback<ListaProveedor>() {
                @Override
                public void onResponse(Call<ListaProveedor> call, Response<ListaProveedor> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaProveedor = response.body();

                        if(listaProveedor.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaProveedorAdapter(getActivity(),listaProveedor.Detalle,ProveedorPrincipalFragment.this);
                            recycler.setAdapter(adapter);
                            pDialog.dismiss();
                        }
                        else if(listaProveedor.Mensaje.Status == eStatus.Error)
                        {
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaProveedor.Mensaje.DetalleMensaje);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaProveedor> call, Throwable t) {
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    private void ObtenerListaProveedores()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Cargando Datos...");
            pDialog.setCancelable(false);
            pDialog.show();

            Call<ListaProveedor> callListarProveedores = mRESTApi.ListarProveedoresPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarProveedores.enqueue(new Callback<ListaProveedor>() {
                @Override
                public void onResponse(Call<ListaProveedor> call, Response<ListaProveedor> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaProveedor = response.body();

                        if(listaProveedor.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaProveedorAdapter(getActivity(),listaProveedor.Detalle,ProveedorPrincipalFragment.this);
                            recycler.setAdapter(adapter);
                            pDialog.dismiss();
                        }
                        else if(listaProveedor.Mensaje.Status == eStatus.Error)
                        {
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaProveedor.Mensaje.DetalleMensaje);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaProveedor> call, Throwable t) {
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                }
            });

        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_UPDATE_DELETE_PROVEEDOR:
                    ObtenerListaProveedores();
                    break;
                case AddEditProveedorActivity.REQUEST_ADD_PROVEEDOR:
                    ObtenerListaProveedores();
                    break;
            }
        }
    }

}
