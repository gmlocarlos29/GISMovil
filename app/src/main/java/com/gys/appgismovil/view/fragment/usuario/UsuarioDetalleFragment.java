package com.gys.appgismovil.view.fragment.usuario;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.activity.usuario.AddEditUsuarioActivity;
import com.gys.appgismovil.view.activity.usuario.UsuarioDetalleActivity;
import com.gys.appgismovil.view.utils.ConstantesUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 25/08/2017.
 */
public class UsuarioDetalleFragment extends Fragment
{
    private static final String ARG_USUARIO_ID = "UsuarioId";
    private String mUsuarioId;

    //private CollapsingToolbarLayout collapser;
    private TextView tvNombre;
    private TextView tvCargo;
    private TextView tvEmail;
    private TextView tvUsername;
    private TextView tvPassword;
    private ImageView avatar;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    public UsuarioDetalleFragment() {
        // Required empty public constructor
    }

    public static UsuarioDetalleFragment newInstance(String usuarioId)
    {
        UsuarioDetalleFragment fragment = new UsuarioDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USUARIO_ID, usuarioId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mUsuarioId = getArguments().getString(ARG_USUARIO_ID);
        }

        setHasOptionsMenu(true);

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  view = inflater.inflate(R.layout.fgt_usuario_detalle, container, false);
        avatar = (ImageView) getActivity().findViewById(R.id.usuarioDetalleActivity_avatar);
        tvNombre = (TextView)view.findViewById(R.id.tv_usuariodetalle_nombre);
        tvCargo = (TextView)view.findViewById(R.id.tv_usuariodetalle_cargo);
        tvEmail = (TextView)view.findViewById(R.id.tv_usuariodetalle_email);
        tvUsername = (TextView)view.findViewById(R.id.tv_usuariodetalle_username);
        tvPassword = (TextView)view.findViewById(R.id.tv_usuariodetalle_password);
        LoadUsuario();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void LoadUsuario()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callObtenerUsuario = mRESTApi.ObtenerUsuarioPorId(mUsuarioId);

        callObtenerUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Usuario objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosUsuario(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    public void MostrarDatosUsuario(Usuario objUsuario)
    {
        tvNombre.setText(objUsuario.getUsu_nombre());
        tvCargo.setText(objUsuario.getUsu_funcion());
        tvEmail.setText(objUsuario.getUsu_correo());
        tvUsername.setText(objUsuario.getUsu_username());
        //tvPassword.setText(objUsuario.getUsu_password());
        tvPassword.setText("******");

        //Obtenemos la imagen a mostrar, en caso el usuario no tenga imagen mostramos la imagen por defecto
        String urlImagenUsuario = objUsuario.getUsu_avatar();
        if(urlImagenUsuario.compareTo("")==0) urlImagenUsuario = "uploads/usuario/default.png";

        Glide.with(this)
                //.load("https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAuOAAAAJDM1YTMzMTNmLTBmM2MtNGFhNi1hZTVlLTc0MjIzNDdmODI4YQ.jpg")
                .load(ConstantesUtils.Servidor + urlImagenUsuario)
                .centerCrop()
                .into(avatar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_edit:
                LoadEditarUsuarioActivity();
                break;
            case R.id.action_delete:
                ConfirmarEliminacionUsuario();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ConfirmarEliminacionUsuario()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Confirmar eliminación");
        pDialog.setContentText("¿Está seguro que desea eliminar el usuario?");
        pDialog.setConfirmText("Sí");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                EliminarUsuario();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    public void EliminarUsuario()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Eliminando Usuario...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callEliminarUsuario = mRESTApi.EliminarUsuario(mUsuarioId);
        callEliminarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Usuario objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());
            }
        });
    }

    public void LoadEditarUsuarioActivity()
    {
        Intent intent = new Intent(getActivity(), AddEditUsuarioActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_USUARIO_ID, mUsuarioId);
        startActivityForResult(intent, UsuarioPrincipalFragment.REQUEST_UPDATE_DELETE_USUARIO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == UsuarioPrincipalFragment.REQUEST_UPDATE_DELETE_USUARIO)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }
}
