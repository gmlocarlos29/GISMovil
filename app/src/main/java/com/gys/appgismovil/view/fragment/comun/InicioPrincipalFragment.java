package com.gys.appgismovil.view.fragment.comun;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import com.gys.appgismovil.R;

import com.gys.appgismovil.data.model.comun.InicioPrueba;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.util.Autenticacion;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.usuario.UsuarioPrincipalFragment;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 24/08/2017.
 */
public class InicioPrincipalFragment extends Fragment
{
    ProgressDialog progressDialog = null;

    Button btnUsuarios;
    Button btnProductos;
    Button btnReportes;
    Button btnVentas;
    ImageButton ibtnEscanearQR;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    //private Usuario objUsuarioAutenticado;
    private InicioPrueba objUsuarioInicioPrueba;

    public InicioPrincipalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_inicio_principal, container, false);
        IniciarControles(view);
        IniciarEventos();
        //Obtenemos el usuario logueado
        //objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getActivity());
        objUsuarioInicioPrueba = Autenticacion.getUsuarioInicioPrueba(getActivity());
        return view;
    }

    private void IniciarControles(View view){
        btnUsuarios = (Button)view.findViewById(R.id.cmnFragmentoInicioPrincipal_btnUsuarios);
        btnProductos = (Button)view.findViewById(R.id.cmnFragmentoInicioPrincipal_btnProductos);
        btnReportes = (Button)view.findViewById(R.id.cmnFragmentoInicioPrincipal_btnReportes);
        btnVentas = (Button)view.findViewById(R.id.cmnFragmentoInicioPrincipal_btnVentas);
        //ibtnEscanearQR = (ImageButton)view.findViewById(R.id.cmnFragmentoInicioPrincipal_ibtnEscanearQR);

        /*
        //Creamos la conexion al servicio REST
        mRestAdapter = new Retrofit.Builder()
                .baseUrl(RESTApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Creamos la conexion a la RestApi de la App
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
        */
    }

    private void IniciarEventos()
    {
        btnUsuarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                final SweetAlertDialog dialogUsuarios =  new SweetAlertDialog(getActivity(),SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                final Handler handlerUsuarios = new Handler();
                dialogUsuarios.setTitleText("Usuarios");
                dialogUsuarios.setContentText("Módulo en desarrollo");
                dialogUsuarios.setCustomImage(R.drawable.ic_account_settings);
                dialogUsuarios.setCancelable(false);
                dialogUsuarios.show();
                handlerUsuarios.postDelayed(new Runnable() {
                    public void run() {
                        dialogUsuarios.dismiss();
                    }
                }, 2000);
                */
                //UsuarioPrincipalFragment fragment = new UsuarioPrincipalFragment();
                //((PrincipalActivity)getActivity()).seleccionarFragment(fragment,true);
                PrincipalActivity activity = ((PrincipalActivity)getActivity());
                activity.seleccionarItem(activity.getMenu().getItem(1));
                activity.getMenu().getItem(1).setChecked(true);
            }
        });
        btnProductos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog dialogProductos =  new SweetAlertDialog(getActivity(),SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                final Handler handlerProductos = new Handler();
                dialogProductos.setTitleText("Productos");
                dialogProductos.setContentText("Módulo en desarrollo");
                dialogProductos.setCustomImage(R.drawable.icon_cart48x48);
                dialogProductos.setCancelable(false);
                dialogProductos.show();
                handlerProductos.postDelayed(new Runnable() {
                    public void run() {
                        dialogProductos.dismiss();
                    }
                }, 2000);
            }
        });
        btnReportes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog dialogReportes =  new SweetAlertDialog(getActivity(),SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                final Handler handlerReportes = new Handler();
                dialogReportes.setTitleText("Reportes");
                dialogReportes.setContentText("Módulo en desarrollo");
                dialogReportes.setCustomImage(R.drawable.icon_chart_bar48x48);
                dialogReportes.setCancelable(false);
                dialogReportes.show();
                handlerReportes.postDelayed(new Runnable() {
                    public void run() {
                        dialogReportes.dismiss();
                    }
                }, 2000);
            }
        });
        btnVentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SweetAlertDialog dialogVentas =  new SweetAlertDialog(getActivity(),SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                final Handler handlerVentas = new Handler();
                dialogVentas.setTitleText("Ventas");
                dialogVentas.setContentText("Módulo en desarrollo");
                dialogVentas.setCustomImage(R.drawable.icon_currency_usd48x48);
                dialogVentas.setCancelable(false);
                dialogVentas.show();
                handlerVentas.postDelayed(new Runnable() {
                    public void run() {
                        dialogVentas.dismiss();
                    }
                }, 2000);
            }
        });

        /*
        ibtnEscanearQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    FragmentIntentIntegrator integrator = new FragmentIntentIntegrator(FragmentoInicioPrincipal3.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                    integrator.setPrompt("Coloque el código QR del producto en el recuadro");
                    integrator.setCameraId(0);
                    //integrator.setBeepEnabled(true);
                    integrator.setBarcodeImageEnabled(false);
                    //Cambiamos la orientacion a Portrait
                    //integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
                    //integrator.setOrientationLocked(false);
                    //Cambiamos la orientacion a Portrait y mostramos el flash activado por defecto
                    //integrator.setCaptureActivity(TorchOnCaptureActivity.class);
                    //integrator.setOrientationLocked(false);
                    //Personalizamos el disenio de nuestro reader
                    integrator.setCaptureActivity(ActivityReaderPersonalizado.class);
                    integrator.setOrientationLocked(false);

                    //Obtenemos la configuracion del escaner guardada si es que existe
                    ConfiguracionEscaner objConfiguracion = SharedPref.getConfiguracionEscaner(getActivity());
                    if(objConfiguracion!=null)
                    {
                        integrator.addExtra(Constantes.CAMERA_FLASH_ON,objConfiguracion.isFlashOn());
                        integrator.addExtra(Constantes.CAMERA_VIBRATION_ON,objConfiguracion.isVibrationOn());
                        integrator.addExtra(Constantes.CAMERA_SOUND_ON,objConfiguracion.isSoundOn());
                    }
                    else
                    {
                        integrator.addExtra(Constantes.CAMERA_FLASH_ON,false);
                        integrator.addExtra(Constantes.CAMERA_VIBRATION_ON,false);
                        integrator.addExtra(Constantes.CAMERA_SOUND_ON,false);
                    }

                    integrator.initiateScan();
                }catch (Exception ex)
                {
                    Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        */
    }
}
