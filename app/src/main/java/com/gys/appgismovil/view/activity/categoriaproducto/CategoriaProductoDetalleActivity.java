package com.gys.appgismovil.view.activity.categoriaproducto;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.categoriaproducto.CategoriaProductoDetalleFragment;

/**
 * Created by USER on 16/10/2017.
 */
public class CategoriaProductoDetalleActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categoriaproducto_detalle);
        setToolbar();
        String id = getIntent().getStringExtra(PrincipalActivity.EXTRA_CATEGORIAPRODUCTO_ID);
        CategoriaProductoDetalleFragment fragment = (CategoriaProductoDetalleFragment)
                getSupportFragmentManager().findFragmentById(R.id.categoriaproductoDetalleActivity_container);
        if (fragment == null) {
            fragment = CategoriaProductoDetalleFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.categoriaproductoDetalleActivity_container, fragment)
                    .commit();
        }
    }

    private void setToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.categoriaproductoDetalleActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_categoriaproducto_detalle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
