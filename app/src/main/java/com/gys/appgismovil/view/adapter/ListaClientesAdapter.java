package com.gys.appgismovil.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.Cliente;

import java.util.List;

/**
 * Created by USER on 04/09/2017.
 */
public class ListaClientesAdapter extends RecyclerView.Adapter<ListaClientesAdapter.ListaClienteViewHolder>
{
    private List<Cliente> items;
    Context context;

    private OnItemClickListener escucha;

    public interface OnItemClickListener
    {
        public void onClick(ListaClienteViewHolder holder, String IdCliente);
    }

    public ListaClientesAdapter(Context context,List<Cliente> items, OnItemClickListener escucha)
    {
        this.items = items;
        this.context = context;
        this.escucha = escucha;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ListaClienteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragmentclientesprincipal_rcvclientes, parent, false);
        return new ListaClienteViewHolder(v,this);
    }

    @Override
    public void onBindViewHolder(ListaClienteViewHolder holder, int position)
    {
        holder.lblNombre.setText(items.get(position).getCli_nombre());
        holder.lblNroDoc.setText("Nro. Documento: "+items.get(position).getCli_docnro());
        holder.lblTelefono.setText("Teléfono: "+items.get(position).getCli_telefono());
    }

    public class ListaClienteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //Campos respectivos de un item
        TextView lblNombre;
        TextView lblNroDoc;
        TextView lblTelefono;

        public ListaClienteViewHolder(View view,ListaClientesAdapter adapter)
        {
            super(view);
            lblNombre = (TextView)view.findViewById(R.id.itemFragmentClientePrincipal_lblNombre);
            lblNroDoc = (TextView)view.findViewById(R.id.itemFragmentClientesPrincipal_lblNroDoc);
            lblTelefono = (TextView)view.findViewById(R.id.itemFragmentClientesPrincipal_lblTelefono);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            escucha.onClick(this,ObtenerIdCliente(getAdapterPosition()));
        }
    }

    private String ObtenerIdCliente(int posicion)
    {
        if(items!=null)
        {
            return items.get(posicion).getCli_id();
        }
        else
        {
            return "";
        }
    }
}
