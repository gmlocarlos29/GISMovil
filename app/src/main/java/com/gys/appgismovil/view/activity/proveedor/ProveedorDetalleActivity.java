package com.gys.appgismovil.view.activity.proveedor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.Proveedor;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.proveedor.ProveedorDetalleFragment;

/**
 * Created by USER on 06/09/2017.
 */
public class ProveedorDetalleActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proveedor_detalle);
        setToolbar();
        String id = getIntent().getStringExtra(PrincipalActivity.EXTRA_PROVEEDOR_ID);
        ProveedorDetalleFragment fragment = (ProveedorDetalleFragment)
                getSupportFragmentManager().findFragmentById(R.id.proveedorDetalleActivity_container);
        if (fragment == null) {
            fragment = ProveedorDetalleFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.proveedorDetalleActivity_container, fragment)
                    .commit();
        }
    }

    private void setToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.proveedorDetalleActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_proveedor_detalle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
