package com.gys.appgismovil.view.fragment.cliente;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.Cliente;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.cliente.AddEditClienteActivity;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 05/09/2017.
 */
public class ClienteDetalleFragment extends Fragment
{
    private static final String ARG_CLIENTE_ID = "ClienteId";
    private String mClienteId;

    private TextView tvNombre;
    private TextView tvNroDoc;
    private TextView tvTelefono;
    private TextView tvCorreo;
    private TextView tvDireccion;
    private TextView tvCiudad;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    public ClienteDetalleFragment() {
        // Required empty public constructor
    }

    public static ClienteDetalleFragment newInstance(String clienteId)
    {
        ClienteDetalleFragment fragment = new ClienteDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CLIENTE_ID, clienteId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
            mClienteId = getArguments().getString(ARG_CLIENTE_ID);

        setHasOptionsMenu(true);

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View  view = inflater.inflate(R.layout.fgt_cliente_detalle, container, false);
        tvNombre = (TextView)view.findViewById(R.id.tv_clientedetalle_nombre);
        tvNroDoc = (TextView)view.findViewById(R.id.tv_clientedetalle_nrodoc);
        tvTelefono = (TextView)view.findViewById(R.id.tv_clientedetalle_telefono);
        tvCorreo = (TextView)view.findViewById(R.id.tv_clientedetalle_correo);
        tvDireccion = (TextView)view.findViewById(R.id.tv_clientedetalle_direccion);
        tvCiudad = (TextView)view.findViewById(R.id.tv_clientedetalle_ciudad);
        LoadCliente();
        return view;
    }

    private void LoadCliente()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Cliente> callObtenerCliente = mRESTApi.ObtenerClientePorId(mClienteId);
        callObtenerCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Cliente objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosCliente(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosCliente(Cliente cliente)
    {
        tvNombre.setText(cliente.getCli_nombre());
        tvNroDoc.setText(cliente.getCli_docnro());
        tvTelefono.setText(cliente.getCli_telefono());
        tvCorreo.setText(cliente.getCli_correo());
        tvDireccion.setText(cliente.getCli_direccion());
        tvCiudad.setText(cliente.getCli_ciudad());
    }

    public void LoadEditarClienteActivity()
    {
        Intent intent = new Intent(getActivity(), AddEditClienteActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_CLIENTE_ID, mClienteId);
        startActivityForResult(intent, ClientePrincipalFragment.REQUEST_UPDATE_DELETE_CLIENTE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ClientePrincipalFragment.REQUEST_UPDATE_DELETE_CLIENTE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_edit:
                LoadEditarClienteActivity();
                break;
            case R.id.action_delete:
                ConfirmarEliminacionCliente();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void ConfirmarEliminacionCliente()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Confirmar eliminación");
        pDialog.setContentText("¿Está seguro que desea eliminar el cliente?");
        pDialog.setConfirmText("Sí");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                EliminarCliente();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    private void EliminarCliente()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Eliminando Cliente...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Cliente> callEliminarCliente = mRESTApi.EliminarCliente(mClienteId);
        callEliminarCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Cliente objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                    }
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());
            }
        });
    }

}
