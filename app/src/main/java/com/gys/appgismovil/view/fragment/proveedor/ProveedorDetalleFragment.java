package com.gys.appgismovil.view.fragment.proveedor;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.Proveedor;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.activity.proveedor.AddEditProveedorActivity;
import com.gys.appgismovil.view.fragment.cliente.ClientePrincipalFragment;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 06/09/2017.
 */
public class ProveedorDetalleFragment extends Fragment
{
    private static final String ARG_PROVEEDOR_ID = "ProveedorId";
    private String mProveedorId;

    private TextView tvRazonSocial;
    private TextView tvNroDoc;
    private TextView tvTelefono;
    private TextView tvCorreo;
    private TextView tvDireccion;
    private TextView tvCiudad;
    private TextView tvContacto;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    public ProveedorDetalleFragment() {
        // Required empty public constructor
    }

    public static ProveedorDetalleFragment newInstance(String proveedorId)
    {
        ProveedorDetalleFragment fragment = new ProveedorDetalleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PROVEEDOR_ID, proveedorId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
            mProveedorId = getArguments().getString(ARG_PROVEEDOR_ID);

        setHasOptionsMenu(true);

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View  view = inflater.inflate(R.layout.fgt_proveedor_detalle, container, false);
        tvRazonSocial = (TextView)view.findViewById(R.id.tv_proveedordetalle_razonsocial);
        tvNroDoc = (TextView)view.findViewById(R.id.tv_proveedordetalle_nrodoc);
        tvTelefono = (TextView)view.findViewById(R.id.tv_proveedordetalle_telefono);
        tvCorreo = (TextView)view.findViewById(R.id.tv_proveedordetalle_correo);
        tvDireccion = (TextView)view.findViewById(R.id.tv_proveedordetalle_direccion);
        tvCiudad = (TextView)view.findViewById(R.id.tv_proveedordetalle_ciudad);
        tvContacto = (TextView)view.findViewById(R.id.tv_proveedordetalle_contacto);
        LoadProveedor();
        return view;
    }

    private void LoadProveedor()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Proveedor> callObtenerProveedor = mRESTApi.ObtenerProveedorPorId(mProveedorId);
        callObtenerProveedor.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Proveedor objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosProveedor(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosProveedor(Proveedor proveedor)
    {
        tvRazonSocial.setText(proveedor.getProv_razonsocial());
        tvNroDoc.setText(proveedor.getProv_docnro());
        tvTelefono.setText(proveedor.getProv_telefono());
        tvCorreo.setText(proveedor.getProv_correo());
        tvDireccion.setText(proveedor.getProv_direccion());
        tvCiudad.setText(proveedor.getProv_ciudad());
        tvContacto.setText(proveedor.getProv_contacto());
    }

    public void LoadEditarProveedorActivity()
    {
        Intent intent = new Intent(getActivity(), AddEditProveedorActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_PROVEEDOR_ID, mProveedorId);
        startActivityForResult(intent, ProveedorPrincipalFragment.REQUEST_UPDATE_DELETE_PROVEEDOR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ProveedorPrincipalFragment.REQUEST_UPDATE_DELETE_PROVEEDOR)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_edit:
                LoadEditarProveedorActivity();
                break;
            case R.id.action_delete:
                ConfirmarEliminacionProveedor();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void ConfirmarEliminacionProveedor()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE);
        pDialog.setTitleText("Confirmar eliminación");
        pDialog.setContentText("¿Está seguro que desea eliminar el proveedor?");
        pDialog.setConfirmText("Sí");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
                EliminarProveedor();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
        pDialog.show();
    }

    private void EliminarProveedor()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Eliminando Proveedor...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Proveedor> callEliminarProveedor = mRESTApi.EliminarProveedor(mProveedorId);
        callEliminarProveedor.enqueue(new Callback<Proveedor>() {
            @Override
            public void onResponse(Call<Proveedor> call, Response<Proveedor> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Proveedor objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                    }
                }
            }

            @Override
            public void onFailure(Call<Proveedor> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());
            }
        });
    }

}
