package com.gys.appgismovil.view.fragment.usuario;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Mensaje;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.comun.ImagenActivity;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConstantesUtils;
import com.gys.appgismovil.view.utils.FechaUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 28/08/2017.
 */
public class AddEditUsuarioFragment extends Fragment
{
    private static final String ARG_USUARIO_ID = "arg_usuario_id";
    private static final String TEMPORAL_PICTURE_NAME = "usuario.jpg";

    private static final int REQUEST_ABRIR_GALERIA = 10;
    private static final int REQUEST_ABRIR_CAMARA = 11;

    private String mUsuarioId;

    private TextInputLayout tilNombre;
    private EditText txtNombre;
    private TextInputLayout tilCargo;
    private EditText txtCargo;
    private TextInputLayout tilEmail;
    private EditText txtEmail;
    private TextInputLayout tilUsername;
    private EditText txtUsername;
    private TextInputLayout tilPassword;
    private EditText txtPassword;
    private ImageButton ibtnAbrirGaleria;
    private ImageButton ibtnAbrirCamara;
    //private ImageView imgUsuario;
    private ImageButton ibtnVerImagen;
    private ImageButton ibtnEliminarImagen;
    TableLayout tblImagen;

    private Usuario objUsuarioEditar = null;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private FloatingActionButton fabGuardar;

    Usuario objUsuarioAutenticado;

    //Variables para la gestion de la imagen
    private Uri mImageUri = null;   // almacena la uri de la imagen en el movil
    private String mediaPath = null;  //almacena el path de la imagen en el movil
    MultipartBody.Part bodyImagen = null; //almacena la imagen del movil a enviar al servidor
    private boolean tieneImagen = false; // variable para controlar si se esta enviando o no imagen

    public AddEditUsuarioFragment() {
        // Required empty public constructor
    }

    public static AddEditUsuarioFragment newInstance(String lawyerId) {
        AddEditUsuarioFragment fragment = new AddEditUsuarioFragment();
        Bundle args = new Bundle();
        args.putString(ARG_USUARIO_ID, lawyerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mUsuarioId = getArguments().getString(ARG_USUARIO_ID);
        }
        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        //Obtenemos el usuario logueado
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fgt_usuario_addedit, container, false);
        IniciarControles(view);

        // Carga de datos
        if (mUsuarioId != null)
        {
            LoadUsuario();
        }

        return view;
    }

    public void IniciarControles(View view)
    {
        tilNombre = (TextInputLayout)view.findViewById(R.id.til_usuarioAddEditFragment_nombre);
        txtNombre = (EditText)view.findViewById(R.id.txt_usuarioAddEditFragment_nombre);
        tilCargo = (TextInputLayout)view.findViewById(R.id.til_usuarioAddEditFragment_cargo);
        txtCargo = (EditText)view.findViewById(R.id.txt_usuarioAddEditFragment_cargo);
        tilEmail = (TextInputLayout)view.findViewById(R.id.til_usuarioAddEditFragment_email);
        txtEmail = (EditText)view.findViewById(R.id.txt_usuarioAddEditFragment_email);
        tilUsername = (TextInputLayout)view.findViewById(R.id.til_usuarioAddEditFragment_username);
        txtUsername = (EditText)view.findViewById(R.id.txt_usuarioAddEditFragment_username);
        tilPassword = (TextInputLayout)view.findViewById(R.id.til_usuarioAddEditFragment_password);
        txtPassword = (EditText)view.findViewById(R.id.txt_usuarioAddEditFragment_password);
        ibtnAbrirGaleria = (ImageButton)view.findViewById(R.id.ibtn_usuarioaddedit_abrirgaleria);
        ibtnAbrirCamara = (ImageButton)view.findViewById(R.id.ibtn_usuarioedit_abrirCamara);
        ibtnVerImagen = (ImageButton)view.findViewById(R.id.ibtn_usuarioaddedit_verimagen);
        ibtnEliminarImagen = (ImageButton)view.findViewById(R.id.ibtn_usuarioaddedit_eliminarimagen);
        tblImagen = (TableLayout)view.findViewById(R.id.tbl_usuarioaddedit_imagen);
        //imgUsuario = (ImageView)view.findViewById(R.id.ibtn_usuarioaddedit_imgUsuario);
        fabGuardar = (FloatingActionButton)getActivity().findViewById(R.id.usuarioAddEditActivity_fabGuardar);

        fabGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarUsuario();
            }
        });

        ibtnAbrirGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirGaleria();
            }
        });
        ibtnAbrirCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirCamara();
            }
        });
        ibtnVerImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarImagenActivity();
            }
        });
        ibtnEliminarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuitarImagen();
            }
        });
    }

    public void MostrarImagenActivity()
    {
        Intent intent = new Intent(getContext(), ImagenActivity.class);
        if(mImageUri!=null)
            intent.putExtra("imageUri",mImageUri.toString());
        else
            intent.putExtra("imageUri",ConstantesUtils.Servidor+objUsuarioEditar.getUsu_avatar());
        startActivity(intent);
    }

    public void QuitarImagen()
    {
        tblImagen.setVisibility(View.GONE);
        mImageUri = null;
        mediaPath = null;
        bodyImagen = null;
        tieneImagen = false;
    }

    public void AbrirGaleria()
    {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent.createChooser(intent,"Selecciona App de imagen"),REQUEST_ABRIR_GALERIA);
    }

    public void AbrirCamara()
    {
        //Creamos una carpeta para guardar la imagen temporal
        //File carpeta = new File(Environment.getExternalStorageDirectory(), "AppTmp");
        //carpeta.mkdirs();

        //Aniadimos el nombre de la imagen
        //File image = new File(carpeta, TEMPORAL_PICTURE_NAME);

        //mImageUri = Uri.fromFile(image);

        //Creamos el Intent para llamar a la Camara
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //Le decimos al Intent que queremos grabar la imagen
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);

        //Fragment fragment = this;

        //Lanzamos la aplicacion de la camara con retorno (forResult)
        startActivityForResult(intent, REQUEST_ABRIR_CAMARA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_ABRIR_GALERIA:
                    mImageUri = data.getData();
                    //imgUsuario.setImageURI(mImageUri);
                    tblImagen.setVisibility(View.VISIBLE);
                    tieneImagen = true;
                    break;
                case REQUEST_ABRIR_CAMARA:
                    mImageUri = data.getData();
                    //imgUsuario.setImageURI(mImageUri);
                    tblImagen.setVisibility(View.VISIBLE);
                    tieneImagen = true;
                    break;
            }
        }
    }

    public void LoadUsuario()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callObtenerUsuario = mRESTApi.ObtenerUsuarioPorId(mUsuarioId);

        callObtenerUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Usuario objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosUsuario(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    public void MostrarDatosUsuario(Usuario objUsuario)
    {
        txtNombre.setText(objUsuario.getUsu_nombre());
        txtCargo.setText(objUsuario.getUsu_funcion());
        txtEmail.setText(objUsuario.getUsu_correo());
        txtUsername.setText(objUsuario.getUsu_username());
        txtPassword.setText(objUsuario.getUsu_password());

        objUsuarioEditar = objUsuario;
        //Creamos la Uri de la imagen del usuario a partir de la Url de la imagen del usuario a editar
        if(objUsuarioEditar.getUsu_avatar().compareTo("")!=0)
        {
            //mImageUri = Uri.parse(ConstantesUtils.Servidor+objUsuarioEditar.getUsu_avatar());
            tblImagen.setVisibility(View.VISIBLE);
            tieneImagen = true;
        }
    }

    public void GuardarUsuario()
    {
        boolean error = false;

        // Obtenemos los datos del usuario
        String nombre = txtNombre.getText().toString();
        String cargo = txtCargo.getText().toString();
        String email = txtEmail.getText().toString();
        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();
        int idEmpresa = objUsuarioAutenticado.getUsu_idempresa();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //String avatar = idEmpresa+"_"+timeStamp + ".jpg";

        String avatar = "";
        //Si se eligio una imagen del dispositivo
        if(mImageUri!=null)
            avatar = idEmpresa+"_"+timeStamp + ".jpg";
        else
        {
            // Si se esta editando el usuario y no se esta enviando imagen desde el movil
            // y el usuario tiene imagen subida anteriormente
            if(mUsuarioId!=null && tieneImagen)
            {
                avatar = objUsuarioEditar.getUsu_avatar();
            }
        }

        //Este atributo permitira eliminar la imagen anterior del usuario
        String avatar_anterior = "";
        if(objUsuarioEditar!=null) avatar_anterior = objUsuarioEditar.getUsu_avatar();
        int anio = FechaUtils.GetAnioActual();


        if (TextUtils.isEmpty(nombre)) {
            txtNombre.setError(getString(R.string.field_error));
            error = true;
        }

        if (TextUtils.isEmpty(cargo)) {
            txtCargo.setError(getString(R.string.field_error));
            error = true;
        }

        if (TextUtils.isEmpty(email)) {
            txtEmail.setError(getString(R.string.field_error));
            error = true;
        }

        if (TextUtils.isEmpty(username)) {
            txtUsername.setError(getString(R.string.field_error));
            error = true;
        }

        if (TextUtils.isEmpty(password)) {
            txtPassword.setError(getString(R.string.field_error));
            error = true;
        }

        if (error) {
            return;
        }

        Usuario usuario = new Usuario(nombre, cargo, email, username, password, idEmpresa, avatar, anio, avatar_anterior);

        if (mUsuarioId != null)
        {
            //Ya que estamos editando el usuario entonces enviamos el id de usuario
            usuario.setUsu_id(mUsuarioId);

            //Creamos el objeto MediaPart de la imagen a partir de la Uri de la imagen
            CrearImagen();

            //Actualizamos los datos del usuario
            ActualizarUsuario2(usuario);
        }
        else
        {
            //Creamos el objeto MediaPart de la imagen a partir de la Uri de la imagen
            CrearImagen();

            //Registramos los datos del usuario
            RegistrarUsuario2(usuario);
        }
    }

    public void CrearImagen2()
    {
        if(mImageUri!=null)
        {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
            assert cursor != null;
            // Si la imagen se encuentra en el dispositivo
            if(cursor!=null)
            {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                //obtenemos la ruta donde esta almacenada la imagen
                mediaPath = cursor.getString(columnIndex);
                cursor.close();

                File file = new File(mediaPath);
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"),file);
                bodyImagen = MultipartBody.Part.createFormData("imagen", file.getName(), requestFile);
            }
        }
    }

    public void CrearImagen()
    {
        if(mImageUri!=null)
        {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
            assert cursor != null;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            //obtenemos la ruta donde esta almacenada la imagen
            mediaPath = cursor.getString(columnIndex);
            cursor.close();
            File file = new File(mediaPath);
            RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"),file);
            bodyImagen = MultipartBody.Part.createFormData("imagen", file.getName(), requestFile);
        }
    }

    public void RegistrarUsuario2(Usuario usuario)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        /*
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        mediaPath = cursor.getString(columnIndex);
        cursor.close();

        File file = new File(mediaPath);
        */
        //RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"),file);
        //MultipartBody.Part body = MultipartBody.Part.createFormData("imagen", file.getName(), requestFile);


        Call<Usuario> callRegistrarUsuario = mRESTApi.RegistrarUsuario2(bodyImagen,usuario);
        callRegistrarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }

                    fabGuardar.setEnabled(true);
                }
            }
            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    public void RegistrarUsuario(Usuario usuario)
    {
        //Toast.makeText(getContext(),"Aqui toca registrar el usuario", Toast.LENGTH_SHORT).show();
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callRegistrarUsuario = mRESTApi.RegistrarUsuario(usuario);
        callRegistrarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        SubirImagen(pDialog,response.body());

                        //pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        //pDialog.setTitleText("Operación Exitosa");
                        //pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        //pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        //Handler handler = new Handler();
                        //handler.postDelayed(new Runnable() {
                        //    public void run() {
                        //        pDialog.dismiss();
                        //        getActivity().setResult(Activity.RESULT_OK);
                        //        getActivity().finish();
                        //    }
                        //}, 3000);

                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }

                    fabGuardar.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    public void ActualizarUsuario2(Usuario usuario)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callActualizarUsuario = mRESTApi.ActualizarUsuario2(bodyImagen,usuario);
        callActualizarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }
                }

                fabGuardar.setEnabled(true);
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    public void ActualizarUsuario(Usuario usuario)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Usuario> callActualizarUsuario = mRESTApi.ActualizarUsuario(usuario);
        callActualizarUsuario.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }
                }

                fabGuardar.setEnabled(true);
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    public void SubirImagen(final SweetAlertDialog pDialog,final Usuario result)
    {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContext().getContentResolver().query(mImageUri, filePathColumn, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        mediaPath = cursor.getString(columnIndex);
        cursor.close();

        File file = new File(mediaPath);

        RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("imagen", file.getName(), requestFile);

        Call<Mensaje> callSubirImagen = mRESTApi.SubitImagen(body);
        callSubirImagen.enqueue(new Callback<Mensaje>() {
            @Override
            public void onResponse(Call<Mensaje> call, Response<Mensaje> response) {
                pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                pDialog.setTitleText("Operación Exitosa");
                pDialog.setContentText(result.Mensaje.DetalleMensaje);
                //pDialog.setContentText(response.body().DetalleMensaje);
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        //getActivity().setResult(Activity.RESULT_OK);
                        //getActivity().finish();
                    }
                }, 3000);
            }

            @Override
            public void onFailure(Call<Mensaje> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

}
