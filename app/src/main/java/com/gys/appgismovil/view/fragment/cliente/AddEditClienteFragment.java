package com.gys.appgismovil.view.fragment.cliente;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.Cliente;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.FechaUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 04/09/2017.
 */
public class AddEditClienteFragment extends Fragment
{
    private static final String ARG_CLIENTE_ID = "arg_cliente_id";

    private String mClienteId;
    private TextInputLayout tilNombre;
    private EditText txtNombre;
    private TextInputLayout tilNroDocumento;
    private EditText txtNroDocumento;
    private TextInputLayout tilTelefono;
    private EditText txtTelefono;
    private TextInputLayout tilEmail;
    private EditText txtEmail;
    private TextInputLayout tilDireccion;
    private EditText txtDireccion;
    private TextInputLayout tilCiudad;
    private EditText txtCiudad;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private FloatingActionButton fabGuardar;

    Usuario objUsuarioAutenticado;

    public AddEditClienteFragment() {
        // Required empty public constructor
    }

    public static AddEditClienteFragment newInstance(String lawyerId) {
        AddEditClienteFragment fragment = new AddEditClienteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CLIENTE_ID, lawyerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            mClienteId = getArguments().getString(ARG_CLIENTE_ID);
        }
        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        //Obtenemos el usuario logueado
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_cliente_addedit, container, false);
        IniciarControles(view);

        // Carga de datos
        if (mClienteId != null)
        {
            LoadCliente();
        }

        return view;
    }

    private void LoadCliente()
    {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Cargando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Cliente> callObtenerCliente = mRESTApi.ObtenerClientePorId(mClienteId);
        callObtenerCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response)
            {
                if(response.isSuccessful() && response.body()!=null)
                {
                    Cliente objResult = response.body();

                    if(objResult.Mensaje.Status == eStatus.Success)
                    {
                        pDialog.dismiss();
                        MostrarDatosCliente(objResult);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Empty)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Datos no encontrados");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(objResult.Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un error");
                        pDialog.setContentText(objResult.Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_CANCELED);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t) {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un error");
                pDialog.setContentText(t.getMessage());
                pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        pDialog.dismiss();
                        getActivity().setResult(Activity.RESULT_CANCELED);
                        getActivity().finish();
                    }
                }, 3000);
            }
        });
    }

    private void MostrarDatosCliente(Cliente cliente)
    {
        txtNombre.setText(cliente.getCli_nombre());
        txtNroDocumento.setText(cliente.getCli_docnro());
        txtTelefono.setText(cliente.getCli_telefono());
        txtEmail.setText(cliente.getCli_correo());
        txtDireccion.setText(cliente.getCli_direccion());
        txtCiudad.setText(cliente.getCli_ciudad());
    }

    private void IniciarControles(View view)
    {
        tilNombre = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_nombre);
        txtNombre = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_nombre);
        tilNroDocumento = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_nrodoc);
        txtNroDocumento = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_nrodoc);
        tilTelefono = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_telefono);
        txtTelefono = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_telefono);
        tilEmail = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_correo);
        txtEmail = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_correo);
        tilDireccion = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_direccion);
        txtDireccion = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_direccion);
        tilCiudad = (TextInputLayout)view.findViewById(R.id.til_clienteAddEditFragment_ciudad);
        txtCiudad = (EditText)view.findViewById(R.id.txt_clienteAddEditFragment_ciudad);

        fabGuardar = (FloatingActionButton)getActivity().findViewById(R.id.clienteAddEditActivity_fabGuardar);

        fabGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarCliente();
            }
        });
    }

    private void GuardarCliente()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            boolean error = false;

            //Obtenemos los datos del cliente
            String nombre = txtNombre.getText().toString();
            String nrodoc = txtNroDocumento.getText().toString();
            String telefono = txtTelefono.getText().toString();
            String email = txtEmail.getText().toString();
            String direccion = txtDireccion.getText().toString();
            String ciudad = txtCiudad.getText().toString();
            int anio = FechaUtils.GetAnioActual();
            int idempresa = objUsuarioAutenticado.getUsu_idempresa();

            if (TextUtils.isEmpty(nombre)) {
                txtNombre.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(nrodoc)) {
                txtNroDocumento.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(telefono)) {
                txtTelefono.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(email)) {
                txtEmail.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(direccion)) {
                txtDireccion.setError(getString(R.string.field_error));
                error = true;
            }
            if (TextUtils.isEmpty(ciudad)) {
                txtCiudad.setError(getString(R.string.field_error));
                error = true;
            }
            if (error) {
                return;
            }

            Cliente cliente = new Cliente(idempresa,nrodoc,nombre,telefono,email,direccion,ciudad,anio);

            if(mClienteId!=null)
            {
                cliente.setCli_id(mClienteId);
                ActualizarCliente(cliente);
            }
            else
            {
                RegistrarCliente(cliente);
            }
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    private void RegistrarCliente(Cliente cliente)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Cliente> callRegistrarCliente = mRESTApi.RegistrarCliente(cliente);
        callRegistrarCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }

                    fabGuardar.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }

    private void ActualizarCliente(Cliente cliente)
    {
        fabGuardar.setEnabled(false);
        final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Guardando Datos...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<Cliente> callActualizarCliente = mRESTApi.ActualizarCliente(cliente);
        callActualizarCliente.enqueue(new Callback<Cliente>() {
            @Override
            public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                if(response.isSuccessful() && response.body()!=null)
                {
                    if(response.body().Mensaje.Status == eStatus.Success)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        pDialog.setTitleText("Operación Exitosa");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                        pDialog.findViewById(R.id.confirm_button).setVisibility(View.GONE);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                pDialog.dismiss();
                                getActivity().setResult(Activity.RESULT_OK);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                    else if(response.body().Mensaje.Status == eStatus.Error)
                    {
                        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        pDialog.setTitleText("Ocurrió un Error");
                        pDialog.setContentText(response.body().Mensaje.DetalleMensaje);
                    }
                }

                fabGuardar.setEnabled(true);
            }

            @Override
            public void onFailure(Call<Cliente> call, Throwable t)
            {
                pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                pDialog.setTitleText("Ocurrió un Error");
                pDialog.setContentText(t.getMessage());

                fabGuardar.setEnabled(true);
            }
        });
    }
}
