package com.gys.appgismovil.view.activity.comun;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.gys.appgismovil.data.model.comun.InicioPrueba;
import com.gys.appgismovil.util.Autenticacion;

/**
 * Created by USER on 29/12/2017.
 */
public class EntryActivity extends AppCompatActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // launch a different activity
        Intent launchIntent = new Intent();
        Class<?> launchActivity;
        try
        {
            String className = getScreenClassName();
            launchActivity = Class.forName(className);
        }
        catch (ClassNotFoundException e)
        {
            launchActivity = InicioPruebaActivity.class;
        }
        launchIntent.setClass(getApplicationContext(), launchActivity);
        startActivity(launchIntent);

        finish();
    }

    /** return Class name of Activity to show **/
    private String getScreenClassName()
    {
        String activity = "";

        InicioPrueba objInicioPrueba = Autenticacion.getUsuarioInicioPrueba(EntryActivity.this);

        if(objInicioPrueba!=null)
        {
            activity = PrincipalActivity.class.getName();
        }
        else
        {
            activity = InicioPruebaActivity.class.getName();
        }

        return activity;
    }


}
