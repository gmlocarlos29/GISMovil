package com.gys.appgismovil.view.fragment.categoriaproducto;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.CategoriaProducto;
import com.gys.appgismovil.data.model.entidades.ListaCategoriaProducto;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.categoriaproducto.AddEditCategoriaProductoActivity;
import com.gys.appgismovil.view.activity.categoriaproducto.CategoriaProductoDetalleActivity;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.adapter.ListaCategoriaProductoAdapter;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 15/10/2017.
 */
public class CategoriaProductoPrincipalFragment extends Fragment implements ListaCategoriaProductoAdapter.OnItemClickListener
{
    private RecyclerView recycler;
    private ListaCategoriaProductoAdapter adapter;
    private RecyclerView.LayoutManager lManager;
    private ListaCategoriaProducto listaCategoriaProducto;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private SwipeRefreshLayout srlListaCategoriaProducto;

    private FloatingActionButton fabNuevaCategoriaProducto;

    public static final int REQUEST_UPDATE_DELETE_CATEGORIAPRODUCTO = 2;

    private Usuario objUsuarioAutenticado;

    public CategoriaProductoPrincipalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_categoriaproducto_principal, container, false);
        IniciarControles(view);
        return view;
    }

    public void IniciarControles(View view)
    {
        // Obtener el Recycler
        recycler = (RecyclerView) view.findViewById(R.id.fragmentCategoriaProductoPrincipal_rcvCategoriaProducto);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        listaCategoriaProducto = new ListaCategoriaProducto();
        adapter = new ListaCategoriaProductoAdapter(getActivity(),new ArrayList<CategoriaProducto>(),this);
        recycler.setAdapter(adapter);

        //Obtenemos el refresh layout
        srlListaCategoriaProducto = (SwipeRefreshLayout)view.findViewById(R.id.srlListaCategoriaProducto);

        // Seteamos los colores que se usarán a lo largo de la animación
        srlListaCategoriaProducto.setColorSchemeResources(
                R.color.s1,
                R.color.s2,
                R.color.s3,
                R.color.s4
        );

        // Iniciar la tarea asíncrona al revelar el indicador
        srlListaCategoriaProducto.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ObtenerListaCategoriaProductoRefresh();
                    }
                }
        );

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        ObtenerListaCategoriaProducto();

        fabNuevaCategoriaProducto = (FloatingActionButton)view.findViewById(R.id.fragmentCategoriaProductoPrincipal_fabNuevaCategoriaProducto);
        fabNuevaCategoriaProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarActivityRegistrarCategoriaProducto();
            }
        });
    }

    public void ObtenerListaCategoriaProductoRefresh()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            Call<ListaCategoriaProducto> callListarCategoria = mRESTApi.ListarCategoriaProductoPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarCategoria.enqueue(new Callback<ListaCategoriaProducto>() {
                @Override
                public void onResponse(Call<ListaCategoriaProducto> call, Response<ListaCategoriaProducto> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaCategoriaProducto = response.body();

                        if(listaCategoriaProducto.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaCategoriaProductoAdapter(getActivity(),listaCategoriaProducto.Detalle,CategoriaProductoPrincipalFragment.this);
                            recycler.setAdapter(adapter);

                            srlListaCategoriaProducto.setRefreshing(false);
                        }
                        else if(listaCategoriaProducto.Mensaje.Status == eStatus.Error)
                        {
                            srlListaCategoriaProducto.setRefreshing(false);

                            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaCategoriaProducto.Mensaje.DetalleMensaje);
                            pDialog.show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaCategoriaProducto> call, Throwable t) {
                    srlListaCategoriaProducto.setRefreshing(false);

                    final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                    pDialog.show();
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    public void ObtenerListaCategoriaProducto()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Cargando Datos...");
            pDialog.setCancelable(false);
            pDialog.show();

            Call<ListaCategoriaProducto> callListarCategoria = mRESTApi.ListarCategoriaProductoPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarCategoria.enqueue(new Callback<ListaCategoriaProducto>() {
                @Override
                public void onResponse(Call<ListaCategoriaProducto> call, Response<ListaCategoriaProducto> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaCategoriaProducto = response.body();

                        if(listaCategoriaProducto.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaCategoriaProductoAdapter(getActivity(),listaCategoriaProducto.Detalle,CategoriaProductoPrincipalFragment.this);
                            recycler.setAdapter(adapter);
                            pDialog.dismiss();
                        }
                        else if(listaCategoriaProducto.Mensaje.Status == eStatus.Error)
                        {
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaCategoriaProducto.Mensaje.DetalleMensaje);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaCategoriaProducto> call, Throwable t) {
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    @Override
    public void onClick(ListaCategoriaProductoAdapter.ListaCategoriaProductoViewHolder holder, int IdCategoriaProducto) {
       MostrarActivityCategoriaProductoDetalle(IdCategoriaProducto);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_UPDATE_DELETE_CATEGORIAPRODUCTO:
                    ObtenerListaCategoriaProducto();
                    break;
                case AddEditCategoriaProductoActivity.REQUEST_ADD_CATEGORIAPRODUCTO:
                    ObtenerListaCategoriaProducto();
                    break;
            }
        }
    }

    public void MostrarActivityCategoriaProductoDetalle(int IdCategoriaProducto)
    {
        Intent intent = new Intent(getActivity(), CategoriaProductoDetalleActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_CATEGORIAPRODUCTO_ID,String.valueOf(IdCategoriaProducto));
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_CATEGORIAPRODUCTO);
    }

    public void MostrarActivityRegistrarCategoriaProducto()
    {
        Intent intent = new Intent(getActivity(), AddEditCategoriaProductoActivity.class);
        startActivityForResult(intent, AddEditCategoriaProductoActivity.REQUEST_ADD_CATEGORIAPRODUCTO);
    }



}
