package com.gys.appgismovil.view.activity.usuario;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.gys.appgismovil.R;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.fragment.usuario.UsuarioDetalleFragment;

public class UsuarioDetalleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_detalle);
        setToolbar();
        //setTitle("Datos del Usuario");
        String id = getIntent().getStringExtra(PrincipalActivity.EXTRA_USUARIO_ID);

        UsuarioDetalleFragment fragment = (UsuarioDetalleFragment)
                getSupportFragmentManager().findFragmentById(R.id.usuarioDetalleActivity_container);
        if (fragment == null) {
            fragment = UsuarioDetalleFragment.newInstance(id);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.usuarioDetalleActivity_container, fragment)
                    .commit();
        }
    }

    public void setToolbar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.usuarioDetalleActivity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_usuario_detalle, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setActionBarTitle(String Title)
    {
        setTitle(Title);
    }
}
