package com.gys.appgismovil.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.CategoriaProducto;

import java.util.List;

/**
 * Created by USER on 15/10/2017.
 */
public class ListaCategoriaProductoAdapter extends RecyclerView.Adapter<ListaCategoriaProductoAdapter.ListaCategoriaProductoViewHolder>
{
    private List<CategoriaProducto> items;
    Context context;

    private OnItemClickListener escucha;

    public interface OnItemClickListener
    {
        public void onClick(ListaCategoriaProductoViewHolder holder, int IdCategoriaProducto);
    }

    public ListaCategoriaProductoAdapter(Context context, List<CategoriaProducto> items, OnItemClickListener escucha)
    {
        this.items = items;
        this.context = context;
        this.escucha = escucha;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ListaCategoriaProductoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragmentcategoriaproductoprincipal_rcvcategoriaproducto, parent, false);
        return new ListaCategoriaProductoViewHolder(v,this);
    }

    @Override
    public void onBindViewHolder(ListaCategoriaProductoViewHolder holder, int position)
    {
        holder.lblNombre.setText(items.get(position).getCatpro_nombre());
    }


    public class ListaCategoriaProductoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //Campos respectivos de un item
        TextView lblNombre;

        public ListaCategoriaProductoViewHolder(View view, ListaCategoriaProductoAdapter adapter)
        {
            super(view);
            lblNombre = (TextView)view.findViewById(R.id.itemFragmentCategoriaProductoPrincipal_lblNombre);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            escucha.onClick(this,ObtenerIdCategoriaProducto(getAdapterPosition()));
        }
    }

    private int ObtenerIdCategoriaProducto(int posicion)
    {
        if(items!=null)
        {
            return items.get(posicion).getCatpro_id();
        }
        else
        {
            return 0;
        }
    }

}
