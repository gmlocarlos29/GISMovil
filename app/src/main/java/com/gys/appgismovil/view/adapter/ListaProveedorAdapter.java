package com.gys.appgismovil.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.entidades.ListaProveedor;
import com.gys.appgismovil.data.model.entidades.Proveedor;

import java.util.List;

/**
 * Created by USER on 06/09/2017.
 */
public class ListaProveedorAdapter extends RecyclerView.Adapter<ListaProveedorAdapter.ListaProveedorViewHolder>
{
    private List<Proveedor> items;
    Context context;

    private OnItemClickListener escucha;

    public interface OnItemClickListener
    {
        public void onClick(ListaProveedorViewHolder holder, String IdProveedor);
    }

    public ListaProveedorAdapter(Context context,List<Proveedor> items, OnItemClickListener escucha)
    {
        this.items = items;
        this.context = context;
        this.escucha = escucha;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ListaProveedorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragmentproveedoresprincipal_rcvproveedores, parent, false);
        return new ListaProveedorViewHolder(v,this);
    }

    @Override
    public void onBindViewHolder(ListaProveedorViewHolder holder, int position)
    {
        holder.lblRazonSocial.setText(items.get(position).getProv_razonsocial());
        holder.lblNroDoc.setText("Nro. Documento: "+items.get(position).getProv_docnro());
        holder.lblTelefono.setText("Teléfono: "+items.get(position).getProv_telefono());
    }

    public class ListaProveedorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        //Campos respectivos de un item
        TextView lblRazonSocial;
        TextView lblNroDoc;
        TextView lblTelefono;

        public ListaProveedorViewHolder(View view,ListaProveedorAdapter adapter)
        {
            super(view);
            lblRazonSocial = (TextView)view.findViewById(R.id.itemFragmentProveedorPrincipal_lblRazonSocial);
            lblNroDoc = (TextView)view.findViewById(R.id.itemFragmentProveedorPrincipal_lblNroDoc);
            lblTelefono = (TextView)view.findViewById(R.id.itemFragmentProveedorPrincipal_lblTelefono);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            escucha.onClick(this,ObtenerIdProveedor(getAdapterPosition()));
        }
    }

    private String ObtenerIdProveedor(int posicion)
    {
        if(items!=null)
        {
            return items.get(posicion).getProv_id();
        }
        else
        {
            return "";
        }
    }
}
