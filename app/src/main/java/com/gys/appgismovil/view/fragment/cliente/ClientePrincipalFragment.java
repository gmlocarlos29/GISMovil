package com.gys.appgismovil.view.fragment.cliente;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gys.appgismovil.R;
import com.gys.appgismovil.data.model.comun.Usuario;
import com.gys.appgismovil.data.model.entidades.Cliente;
import com.gys.appgismovil.data.model.entidades.ListaCliente;
import com.gys.appgismovil.enumeradores.eStatus;
import com.gys.appgismovil.view.activity.cliente.AddEditClienteActivity;
import com.gys.appgismovil.view.activity.cliente.ClienteDetalleActivity;
import com.gys.appgismovil.view.activity.comun.PrincipalActivity;
import com.gys.appgismovil.view.adapter.ListaClientesAdapter;
import com.gys.appgismovil.view.utils.AutenticacionUtils;
import com.gys.appgismovil.view.utils.ConectividadUtils;
import com.gys.appgismovil.view.utils.RESTApiUtils;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 04/09/2017.
 */
public class ClientePrincipalFragment extends Fragment implements ListaClientesAdapter.OnItemClickListener
{
    private RecyclerView recycler;
    private ListaClientesAdapter adapter;
    private RecyclerView.LayoutManager lManager;
    private ListaCliente listaCliente;

    private Retrofit mRestAdapter;
    private RESTApiUtils mRESTApi;

    private SwipeRefreshLayout srlListaClientes;

    private FloatingActionButton fabNuevoCliente;

    public static final int REQUEST_UPDATE_DELETE_CLIENTE = 2;

    private Usuario objUsuarioAutenticado;

    public ClientePrincipalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objUsuarioAutenticado = AutenticacionUtils.getUsuarioAutenticado(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fgt_clientes_principal, container, false);
        IniciarControles(view);
        return view;
    }

    public void IniciarControles(View view)
    {
        // Obtener el Recycler
        recycler = (RecyclerView) view.findViewById(R.id.fragmentClientesPrincipal_rcvClientes);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        listaCliente = new ListaCliente();
        adapter = new ListaClientesAdapter(getActivity(),new ArrayList<Cliente>(),this);
        recycler.setAdapter(adapter);

        //Obtenemos el refresh layout
        srlListaClientes = (SwipeRefreshLayout)view.findViewById(R.id.srlListaClientes);

        // Seteamos los colores que se usarán a lo largo de la animación
        srlListaClientes.setColorSchemeResources(
                R.color.s1,
                R.color.s2,
                R.color.s3,
                R.color.s4
        );

        // Iniciar la tarea asíncrona al revelar el indicador
        srlListaClientes.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ObtenerListaClientesRefresh();
                    }
                }
        );

        //Creamos la conexion al servicio REST y a la RESTApi de la App
        mRestAdapter = new Retrofit.Builder().baseUrl(RESTApiUtils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        mRESTApi = mRestAdapter.create(RESTApiUtils.class);

        ObtenerListaClientes();

        fabNuevoCliente = (FloatingActionButton)view.findViewById(R.id.fragmentClientesPrincipal_fabNuevoCliente);
        fabNuevoCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MostrarActivityRegistrarCliente();
            }
        });
    }


    public void ObtenerListaClientesRefresh()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            Call<ListaCliente> callListarClientes = mRESTApi.ListarClientesPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarClientes.enqueue(new Callback<ListaCliente>() {
                @Override
                public void onResponse(Call<ListaCliente> call, Response<ListaCliente> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaCliente = response.body();

                        if(listaCliente.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaClientesAdapter(getActivity(),listaCliente.Detalle,ClientePrincipalFragment.this);
                            recycler.setAdapter(adapter);

                            srlListaClientes.setRefreshing(false);
                        }
                        else if(listaCliente.Mensaje.Status == eStatus.Error)
                        {
                            srlListaClientes.setRefreshing(false);

                            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaCliente.Mensaje.DetalleMensaje);
                            pDialog.show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaCliente> call, Throwable t) {
                    srlListaClientes.setRefreshing(false);

                    final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                    pDialog.show();
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    public void ObtenerListaClientes()
    {
        if(ConectividadUtils.conectadoInternet(getContext()))
        {
            final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Cargando Datos...");
            pDialog.setCancelable(false);
            pDialog.show();

            Call<ListaCliente> callListarClientes = mRESTApi.ListarClientesPorEmpresa(objUsuarioAutenticado.getUsu_idempresa());
            callListarClientes.enqueue(new Callback<ListaCliente>() {
                @Override
                public void onResponse(Call<ListaCliente> call, Response<ListaCliente> response) {
                    if(response.isSuccessful() && response.body()!=null)
                    {
                        listaCliente = response.body();

                        if(listaCliente.Mensaje.Status == eStatus.Success)
                        {
                            adapter = new ListaClientesAdapter(getActivity(),listaCliente.Detalle,ClientePrincipalFragment.this);
                            recycler.setAdapter(adapter);
                            pDialog.dismiss();
                        }
                        else if(listaCliente.Mensaje.Status == eStatus.Error)
                        {
                            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            pDialog.setTitleText("Ocurrió un Error");
                            pDialog.setContentText(listaCliente.Mensaje.DetalleMensaje);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ListaCliente> call, Throwable t) {
                    pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    pDialog.setTitleText("Ocurrió un Error");
                    pDialog.setContentText(t.getMessage());
                }
            });
        }
        else
        {
            ConectividadUtils.MostrarAlertaSinConexion(getContext());
        }
    }

    @Override
    public void onClick(ListaClientesAdapter.ListaClienteViewHolder holder, String IdCliente) {
        MostrarActivityClienteDetalle(IdCliente);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_UPDATE_DELETE_CLIENTE:
                    ObtenerListaClientes();
                    break;
                case AddEditClienteActivity.REQUEST_ADD_CLIENTE:
                     ObtenerListaClientes();
                     break;
            }
        }
    }

    public void MostrarActivityClienteDetalle(String IdCliente)
    {
        Intent intent = new Intent(getActivity(), ClienteDetalleActivity.class);
        intent.putExtra(PrincipalActivity.EXTRA_CLIENTE_ID, IdCliente);
        startActivityForResult(intent, REQUEST_UPDATE_DELETE_CLIENTE);
    }

    public void MostrarActivityRegistrarCliente()
    {
        Intent intent = new Intent(getActivity(), AddEditClienteActivity.class);
        startActivityForResult(intent, AddEditClienteActivity.REQUEST_ADD_CLIENTE);
    }

}
