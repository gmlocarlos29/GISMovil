package com.gys.appgismovil.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.gys.appgismovil.data.model.comun.InicioPrueba;

/**
 * Created by USER on 29/12/2017.
 */
public class Autenticacion
{
    public static int setUsuarioInicioPrueba(Context contexto, InicioPrueba inicioPrueba)
    {
        SharedPreferences pref = contexto.getSharedPreferences("UsuarioInicioPrueba", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("NombreApellidos", inicioPrueba.getNombreApellidos());
        editor.putString("NroDocumentoEmpresa", inicioPrueba.getNroDocumentoEmpresa());
        editor.putString("NombreEmpresa", inicioPrueba.getNombreEmpresa());
        editor.putString("CargoEmpresa",inicioPrueba.getCargoEmpresa());
        editor.putInt("IdEmpresa",inicioPrueba.getIdEmpresa());
        editor.putInt("IdTienda",inicioPrueba.getIdTienda());
        editor.putInt("IdUsuario",inicioPrueba.getIdUsuario());
        editor.commit(); // commit changes
        return 1;
    }

    public static InicioPrueba getUsuarioInicioPrueba(Context contexto)
    {
        InicioPrueba objInicioPrueba = null;

        SharedPreferences pref = contexto.getSharedPreferences("UsuarioInicioPrueba",0); // 0 - for private mode

        if(pref.contains("NombreApellidos"))
        {
            objInicioPrueba = new InicioPrueba();

            objInicioPrueba.setNombreApellidos(pref.getString("NombreApellidos",""));
            objInicioPrueba.setNroDocumentoEmpresa(pref.getString("NroDocumentoEmpresa",""));
            objInicioPrueba.setNombreEmpresa(pref.getString("NombreEmpresa",""));
            objInicioPrueba.setCargoEmpresa(pref.getString("CargoEmpresa",""));
            objInicioPrueba.setIdEmpresa(pref.getInt("IdEmpresa",0));
            objInicioPrueba.setIdTienda(pref.getInt("IdTienda",0));
            objInicioPrueba.setIdUsuario(pref.getInt("IdUsuario",0));
        }

        return objInicioPrueba;
    }
}
