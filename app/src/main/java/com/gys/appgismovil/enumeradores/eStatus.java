package com.gys.appgismovil.enumeradores;

/**
 * Created by USER on 28/08/2017.
 */
public class eStatus
{
    public static int Warning = -2;
    public static int Error = -1;
    public static int Empty = 0;
    public static int Success = 1;
    public static int Waiting = 2;
}