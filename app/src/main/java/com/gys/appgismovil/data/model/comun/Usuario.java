package com.gys.appgismovil.data.model.comun;

import java.util.Date;

/**
 * Created by USER on 23/08/2017.
 */
public class Usuario extends Entidad
{
    private String usu_id;
    private int usu_idempresa;
    private String usu_docempresa;
    private String usu_nomempresa;
    private String usu_username;
    private String usu_password;
    private String usu_nombre;
    private String usu_funcion;
    private String usu_correo;
    private String usu_avatar;
    private String usu_estado;

    private String usu_avatar_anterior; // esto sirve para eliminar la imagen anterior al editar un usuario

    public Usuario(){}

    public Usuario(String usu_nombre, String usu_funcion,String usu_correo, String usu_username,String usu_password,
                   int usu_idempresa, String usu_avatar, int anio, String usu_avatar_anterior)
    {
        this.usu_nombre = usu_nombre;
        this.usu_funcion = usu_funcion;
        this.usu_correo = usu_correo;
        this.usu_username = usu_username;
        this.usu_password = usu_password;
        this.usu_idempresa = usu_idempresa;
        this.usu_avatar = usu_avatar;
        this.Anio = anio;
        this.usu_avatar_anterior = usu_avatar_anterior;
    }

    public String getUsu_id() {
        return usu_id;
    }

    public void setUsu_id(String usu_id) {
        this.usu_id = usu_id;
    }

    public int getUsu_idempresa() {
        return usu_idempresa;
    }

    public void setUsu_idempresa(int usu_idempresa) {
        this.usu_idempresa = usu_idempresa;
    }

    public String getUsu_docempresa() {
        return usu_docempresa;
    }

    public void setUsu_docempresa(String usu_docempresa) {
        this.usu_docempresa = usu_docempresa;
    }

    public String getUsu_nomempresa() {
        return usu_nomempresa;
    }

    public void setUsu_nomempresa(String usu_nomempresa) {
        this.usu_nomempresa = usu_nomempresa;
    }

    public String getUsu_username() {
        return usu_username;
    }

    public void setUsu_username(String usu_username) {
        this.usu_username = usu_username;
    }

    public String getUsu_password() {
        return usu_password;
    }

    public void setUsu_password(String usu_password) {
        this.usu_password = usu_password;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getUsu_funcion() {
        return usu_funcion;
    }

    public void setUsu_funcion(String usu_funcion) {
        this.usu_funcion = usu_funcion;
    }

    public String getUsu_correo() {
        return usu_correo;
    }

    public void setUsu_correo(String usu_correo) {
        this.usu_correo = usu_correo;
    }

    public String getUsu_avatar() {
        return usu_avatar;
    }

    public void setUsu_avatar(String usu_avatar) {
        this.usu_avatar = usu_avatar;
    }

    public String getUsu_estado() {
        return usu_estado;
    }

    public void setUsu_estado(String usu_estado) {
        this.usu_estado = usu_estado;
    }

    public String getUsu_avatar_anterior() {
        return usu_avatar_anterior;
    }

    public void setUsu_avatar_anterior(String usu_avatar_anterior) {
        this.usu_avatar_anterior = usu_avatar_anterior;
    }
}
