package com.gys.appgismovil.data.model.entidades;

import com.gys.appgismovil.data.model.comun.Entidad;

/**
 * Created by USER on 17/10/2017.
 */
public class Producto extends Entidad
{
    private String pro_id;
    private int pro_idempresa;
    private int pro_idcategoria;
    private String pro_nombre;
    private String pro_descripcion;
    private String pro_marca;
    private String pro_modelo;
    private int pro_stockactual;
    private int pro_stockmin;
    private int pro_stockmax;
    private float pro_preciocompra;
    private float pro_precioventa;
    private String pro_imagen;
    private int pro_estado;

    public Producto(int pro_idempresa, int pro_idcategoria, String pro_nombre, String pro_descripcion, String pro_marca,
                    String pro_modelo, int pro_stockmin, int pro_stockmax, float pro_preciocompra, float pro_precioventa,
                    String pro_imagen)
    {
        this.setPro_idempresa(pro_idempresa);
        this.setPro_idcategoria(pro_idcategoria);
        this.setPro_nombre(pro_nombre);
        this.setPro_descripcion(pro_descripcion);
        this.setPro_marca(pro_marca);
        this.setPro_modelo(pro_modelo);
        this.setPro_stockmin(pro_stockmin);
        this.setPro_stockmax(pro_stockmax);
        this.setPro_preciocompra(pro_preciocompra);
        this.setPro_precioventa(pro_precioventa);
        this.setPro_imagen(pro_imagen);
    }


    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public int getPro_idempresa() {
        return pro_idempresa;
    }

    public void setPro_idempresa(int pro_idempresa) {
        this.pro_idempresa = pro_idempresa;
    }

    public int getPro_idcategoria() {
        return pro_idcategoria;
    }

    public void setPro_idcategoria(int pro_idcategoria) {
        this.pro_idcategoria = pro_idcategoria;
    }

    public String getPro_nombre() {
        return pro_nombre;
    }

    public void setPro_nombre(String pro_nombre) {
        this.pro_nombre = pro_nombre;
    }

    public String getPro_descripcion() {
        return pro_descripcion;
    }

    public void setPro_descripcion(String pro_descripcion) {
        this.pro_descripcion = pro_descripcion;
    }

    public String getPro_marca() {
        return pro_marca;
    }

    public void setPro_marca(String pro_marca) {
        this.pro_marca = pro_marca;
    }

    public String getPro_modelo() {
        return pro_modelo;
    }

    public void setPro_modelo(String pro_modelo) {
        this.pro_modelo = pro_modelo;
    }

    public int getPro_stockactual() {
        return pro_stockactual;
    }

    public void setPro_stockactual(int pro_stockactual) {
        this.pro_stockactual = pro_stockactual;
    }

    public int getPro_stockmin() {
        return pro_stockmin;
    }

    public void setPro_stockmin(int pro_stockmin) {
        this.pro_stockmin = pro_stockmin;
    }

    public int getPro_stockmax() {
        return pro_stockmax;
    }

    public void setPro_stockmax(int pro_stockmax) {
        this.pro_stockmax = pro_stockmax;
    }

    public float getPro_preciocompra() {
        return pro_preciocompra;
    }

    public void setPro_preciocompra(float pro_preciocompra) {
        this.pro_preciocompra = pro_preciocompra;
    }

    public float getPro_precioventa() {
        return pro_precioventa;
    }

    public void setPro_precioventa(float pro_precioventa) {
        this.pro_precioventa = pro_precioventa;
    }

    public String getPro_imagen() {
        return pro_imagen;
    }

    public void setPro_imagen(String pro_imagen) {
        this.pro_imagen = pro_imagen;
    }

    public int getPro_estado() {
        return pro_estado;
    }

    public void setPro_estado(int pro_estado) {
        this.pro_estado = pro_estado;
    }
}
