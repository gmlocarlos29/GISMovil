package com.gys.appgismovil.data.sqlite;

import android.net.Uri;

/**
 * Created by USER on 29/12/2017.
 */
public class Contrato
{
    public static final String AUTORIDAD = "com.gys.appgismovil";
    public static final Uri URI_BASE = Uri.parse("content://" + AUTORIDAD);

    private static final String RUTA_EMPRESAS = "empresas";
    private static final String RUTA_TIENDAS = "tiendas";
    private static final String RUTA_USUARIOS = "usuarios";

    // Tipos MIME
    public static final String BASE_CONTENIDOS = "appgismovil.";

    public static final String TIPO_CONTENIDO = "vnd.android.cursor.dir/vnd." + BASE_CONTENIDOS;

    public static final String TIPO_CONTENIDO_ITEM = "vnd.android.cursor.item/vnd." + BASE_CONTENIDOS;

    public static String generarMime(String id)
    {
        if (id != null) {
            return TIPO_CONTENIDO + id;
        } else {
            return null;
        }
    }

    public static String generarMimeItem(String id)
    {
        if (id != null) {
            return TIPO_CONTENIDO_ITEM + id;
        } else {
            return null;
        }
    }

    interface ColumnasEmpresa
    {
        String EMP_ID = "emp_id";
        String EMP_DOCTIPO = "emp_doctipo";
        String EMP_DOCNRO = "emp_docnro";
        String EMP_RAZONSOCIAL = "emp_razonsocial";
        String EMP_TELEFONO = "emp_telefono";
        String EMP_CORREO = "emp_correo";
        String EMP_DIRECCION = "emp_direccion";
        String EMP_CIUDAD = "emp_ciudad";
        String EMP_ADMINISTRADOR = "emp_administrador";
        String EMP_PLANAPP = "emp_planapp";
        String EMP_PLANREGISTRO = "emp_planregistro";
        String EMP_PLANVENCIMIENTO = "emp_planvencimiento";
        String EMP_MAXUSER = "emp_maxuser";
        String EMP_CANUSER = "emp_canuser";
        String EMP_ESTADO = "emp_estado";
    }

    interface ColumnasTienda
    {
        String TND_ID = "tnd_id";
        String TND_NOMBRE = "tnd_nombre";
        String TND_IDEMPRESA = "tnd_idempresa";
        String TND_ESTADO = "tnd_estado";
    }

    interface ColumnasUsuario
    {
        String USU_ID = "usu_";
        String USU_IDEMPRESA = "usu_idempresa";
        String USU_USERNAME = "usu_username";
        String USU_PASSWORD = "usu_password";
        String USU_NOMBRE = "usu_nombre";
        String USU_FUNCION = "usu_funcion";
        String USU_CORREO = "usu_correo";
        String USU_AVATAR = "usu_avatar";
        String USU_ESTADO = "usu_estado";
    }

    public static class ContratoEmpresa implements ColumnasEmpresa
    {
        public static final Uri URI_CONTENIDO = URI_BASE.buildUpon().appendPath(RUTA_EMPRESAS).build();

        public static Uri crearUriEmpresa(String id)
        {
            return URI_CONTENIDO.buildUpon().appendPath(id).build();
        }

        public static String obtenerIdEmpresa(Uri uri)
        {
            return uri.getPathSegments().get(1);
        }
    }

    public static class ContratoTienda implements ColumnasTienda
    {
        public static final Uri URI_CONTENIDO = URI_BASE.buildUpon().appendPath(RUTA_TIENDAS).build();

        public static Uri crearUriTienda(String id)
        {
            return URI_CONTENIDO.buildUpon().appendPath(id).build();
        }

        public static String obtenerIdTienda(Uri uri)
        {
            return uri.getPathSegments().get(1);
        }
    }

    public static class ContratoUsuario implements ColumnasUsuario
    {
        public static final Uri URI_CONTENIDO = URI_BASE.buildUpon().appendPath(RUTA_USUARIOS).build();

        public static Uri crearUriUsuario(String id)
        {
            return URI_CONTENIDO.buildUpon().appendPath(id).build();
        }

        public static String obtenerIdUsuario(Uri uri)
        {
            return uri.getPathSegments().get(1);
        }
    }
}
