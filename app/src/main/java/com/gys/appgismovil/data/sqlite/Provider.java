package com.gys.appgismovil.data.sqlite;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by USER on 29/12/2017.
 */
public class Provider extends ContentProvider
{
    public static final String URI_NO_SOPORTADA = "Uri no soportada";

    private BaseDatos helper;
    private ContentResolver resolver;

    public static final UriMatcher uriMatcher;

    //Casos
    public static final int EMPRESAS = 100;
    public static final int EMPRESAS_ID = 101;

    public static final int TIENDAS = 200;
    public static final int TIENDAS_ID = 201;

    public static final int USUARIOS = 300;
    public static final int USUARIOS_ID = 301;

    public static final String AUTORIDAD = "com.gys.appgismovil";

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(AUTORIDAD, "empresas", EMPRESAS);
        uriMatcher.addURI(AUTORIDAD, "empresas/*", EMPRESAS_ID);

        uriMatcher.addURI(AUTORIDAD, "tiendas", TIENDAS);
        uriMatcher.addURI(AUTORIDAD, "tiendas/*", TIENDAS_ID);

        uriMatcher.addURI(AUTORIDAD, "usuarios", USUARIOS);
        uriMatcher.addURI(AUTORIDAD, "usuarios/*", USUARIOS_ID);
    }

    public Provider()
    {

    }

    @Override
    public boolean onCreate()
    {
        helper = new BaseDatos(getContext());
        resolver = getContext().getContentResolver();
        return true;
    }

    @Override
    public String getType(Uri uri)
    {
        switch (uriMatcher.match(uri))
        {
            case EMPRESAS:
                return Contrato.generarMime("empresa");
            case EMPRESAS_ID:
                return Contrato.generarMimeItem("empresa");
            case TIENDAS:
                return Contrato.generarMime("tienda");
            case TIENDAS_ID:
                return Contrato.generarMimeItem("tienda");
            case USUARIOS:
                return Contrato.generarMime("usuario");
            case USUARIOS_ID:
                return Contrato.generarMimeItem("usuario");
            default:
                throw new UnsupportedOperationException("Uri desconocida =>" + uri);
        }
    }

    private void notificarCambio(Uri uri)
    {
        //resolver.notifyChange(uri, null);
        // el tercer parametro indicar si en sync adapter sera ejecutado automaticamente
        // al momento que el content provider cambie
        // en la version notifyChange de dos parametros esta habilitada la sincronizacion
        // automatica por defecto
        resolver.notifyChange(uri,null,false);
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException
    {
        final SQLiteDatabase db = helper.getWritableDatabase();

        db.beginTransaction();
        try
        {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++)
            {
                results[i] = operations.get(i).apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally
        {
            db.endTransaction();
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // Obtener la base de datos
        SQLiteDatabase bd = helper.getWritableDatabase();

        // Comparar URI
        int match = uriMatcher.match(uri);

        long id;

        switch (match)
        {
            case EMPRESAS:
                id = bd.insertOrThrow(BaseDatos.Tablas.EMPRESA, null, values);
                notificarCambio(uri);
                return Contrato.ContratoEmpresa.crearUriEmpresa(id + "");
            case TIENDAS:
                id = bd.insertOrThrow(BaseDatos.Tablas.TIENDA, null, values);
                notificarCambio(uri);
                return Contrato.ContratoTienda.crearUriTienda(id + "");
            case USUARIOS:
                id = bd.insertOrThrow(BaseDatos.Tablas.USUARIO, null, values);
                notificarCambio(uri);
                return Contrato.ContratoUsuario.crearUriUsuario(id + "");
            default:
                throw new UnsupportedOperationException(URI_NO_SOPORTADA);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,String[] selectionArgs, String sortOrder)
    {
        // Obtener la base de datos
        SQLiteDatabase bd = helper.getReadableDatabase();

        // Comparar URI
        int match = uriMatcher.match(uri);

        // string auxiliar para los ids
        String id;

        Cursor c = null;

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        switch (match)
        {
            case EMPRESAS:
                c = bd.query(BaseDatos.Tablas.EMPRESA,projection,selection,selectionArgs,null,null,null);
                c.setNotificationUri(resolver, Contrato.ContratoEmpresa.URI_CONTENIDO);
                break;
            case TIENDAS:
                c = bd.query(BaseDatos.Tablas.TIENDA,projection,selection,selectionArgs,null,null,null);
                c.setNotificationUri(resolver, Contrato.ContratoTienda.URI_CONTENIDO);
                break;
            case USUARIOS:
                c = bd.query(BaseDatos.Tablas.USUARIO,projection,selection,selectionArgs,null,null,null);
                c.setNotificationUri(resolver, Contrato.ContratoUsuario.URI_CONTENIDO);
                break;
            default:
                throw new UnsupportedOperationException(URI_NO_SOPORTADA);
        }

        return c;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs)
    {
        SQLiteDatabase bd = helper.getWritableDatabase();
        String id;
        int afectados;

        switch (uriMatcher.match(uri))
        {
            case EMPRESAS_ID:
                id = Contrato.ContratoEmpresa.obtenerIdEmpresa(uri);
                afectados = bd.update(BaseDatos.Tablas.EMPRESA,values, Contrato.ContratoEmpresa.EMP_ID + " = ?",new String[]{id});
                notificarCambio(uri);
                break;
            case TIENDAS_ID:
                id = Contrato.ContratoTienda.obtenerIdTienda(uri);
                afectados = bd.update(BaseDatos.Tablas.TIENDA,values, Contrato.ContratoTienda.TND_ID + " = ?",new String[]{id});
                notificarCambio(uri);
                break;
            case USUARIOS_ID:
                id = Contrato.ContratoUsuario.obtenerIdUsuario(uri);
                afectados = bd.update(BaseDatos.Tablas.USUARIO,values, Contrato.ContratoUsuario.USU_ID  + " = ?",new String[]{id});
                notificarCambio(uri);
                break;
            default:
                throw new UnsupportedOperationException(URI_NO_SOPORTADA);
        }

        return afectados;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
        SQLiteDatabase bd = helper.getWritableDatabase();
        String id;
        int afectados;

        switch (uriMatcher.match(uri))
        {
            default:
                throw new UnsupportedOperationException(URI_NO_SOPORTADA);
        }
    }
}
