package com.gys.appgismovil.data.model.entidades;

import com.gys.appgismovil.data.model.comun.Entidad;

/**
 * Created by USER on 06/09/2017.
 */
public class Proveedor extends Entidad
{
    private String prov_id;
    private int prov_idempresa;
    private String prov_docnro;
    private String prov_razonsocial;
    private String prov_telefono;
    private String prov_correo;
    private String prov_direccion;
    private String prov_ciudad;
    private String prov_contacto;
    private int prov_estado;

    public Proveedor(int prov_idempresa,String prov_docnro,String prov_razonsocial,String prov_telefono,String prov_correo,
                     String prov_direccion, String prov_ciudad, String prov_contacto, int anio)
    {
        this.prov_idempresa = prov_idempresa;
        this.prov_docnro = prov_docnro;
        this.prov_razonsocial = prov_razonsocial;
        this.prov_telefono = prov_telefono;
        this.prov_correo = prov_correo;
        this.prov_direccion = prov_direccion;
        this.prov_ciudad = prov_ciudad;
        this.prov_contacto = prov_contacto;
        this.Anio = anio;
    }

    public String getProv_id() {
        return prov_id;
    }

    public void setProv_id(String prov_id) {
        this.prov_id = prov_id;
    }

    public int getProv_idempresa() {
        return prov_idempresa;
    }

    public void setProv_idempresa(int prov_idempresa) {
        this.prov_idempresa = prov_idempresa;
    }

    public String getProv_docnro() {
        return prov_docnro;
    }

    public void setProv_docnro(String prov_docnro) {
        this.prov_docnro = prov_docnro;
    }

    public String getProv_razonsocial() {
        return prov_razonsocial;
    }

    public void setProv_razonsocial(String prov_razonsocial) {
        this.prov_razonsocial = prov_razonsocial;
    }

    public String getProv_telefono() {
        return prov_telefono;
    }

    public void setProv_telefono(String prov_telefono) {
        this.prov_telefono = prov_telefono;
    }

    public String getProv_correo() {
        return prov_correo;
    }

    public void setProv_correo(String prov_correo) {
        this.prov_correo = prov_correo;
    }

    public String getProv_direccion() {
        return prov_direccion;
    }

    public void setProv_direccion(String prov_direccion) {
        this.prov_direccion = prov_direccion;
    }

    public String getProv_ciudad() {
        return prov_ciudad;
    }

    public void setProv_ciudad(String prov_ciudad) {
        this.prov_ciudad = prov_ciudad;
    }

    public String getProv_contacto() {
        return prov_contacto;
    }

    public void setProv_contacto(String prov_contacto) {
        this.prov_contacto = prov_contacto;
    }

    public int getProv_estado() {
        return prov_estado;
    }

    public void setProv_estado(int prov_estado) {
        this.prov_estado = prov_estado;
    }
}
