package com.gys.appgismovil.data.model.comun;

/**
 * Created by USER on 23/08/2017.
 */
public class LoginBody
{

    private String documento;
    private String usuario;
    private String password;

    public LoginBody(String documento, String usuario,String password)
    {
        this.setDocumento(documento);
        this.setUsuario(usuario);
        this.setPassword(password);
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
