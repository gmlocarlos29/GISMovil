package com.gys.appgismovil.data.model.entidades;

import com.gys.appgismovil.data.model.comun.Entidad;

/**
 * Created by USER on 15/10/2017.
 */
public class CategoriaProducto extends Entidad
{
    private int catpro_id;
    private String catpro_nombre;
    private int catpro_idempresa;
    private String catpro_descripcion;
    private int catpro_estado;

    public CategoriaProducto(String catpro_nombre, int catpro_idempresa, String catpro_descripcion)
    {
        this.setCatpro_nombre(catpro_nombre);
        this.setCatpro_idempresa(catpro_idempresa);
        this.setCatpro_descripcion(catpro_descripcion);
    }

    public int getCatpro_id() {
        return catpro_id;
    }

    public void setCatpro_id(int catpro_id) {
        this.catpro_id = catpro_id;
    }

    public String getCatpro_nombre() {
        return catpro_nombre;
    }

    public void setCatpro_nombre(String catpro_nombre) {
        this.catpro_nombre = catpro_nombre;
    }

    public int getCatpro_idempresa() {
        return catpro_idempresa;
    }

    public void setCatpro_idempresa(int catpro_idempresa) {
        this.catpro_idempresa = catpro_idempresa;
    }

    public String getCatpro_descripcion() {
        return catpro_descripcion;
    }

    public void setCatpro_descripcion(String catpro_descripcion) {
        this.catpro_descripcion = catpro_descripcion;
    }

    public int getCatpro_estado() {
        return catpro_estado;
    }

    public void setCatpro_estado(int catpro_estado) {
        this.catpro_estado = catpro_estado;
    }
}
