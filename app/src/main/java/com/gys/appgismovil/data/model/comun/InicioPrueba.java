package com.gys.appgismovil.data.model.comun;

/**
 * Created by USER on 29/12/2017.
 */
public class InicioPrueba
{
    private String NombreApellidos;
    private String NroDocumentoEmpresa;
    private String NombreEmpresa;
    private String CargoEmpresa;

    private int IdEmpresa;
    private int IdTienda;
    private int IdUsuario;


    public String getNombreApellidos() {
        return NombreApellidos;
    }

    public void setNombreApellidos(String nombreApellidos) {
        NombreApellidos = nombreApellidos;
    }

    public String getNroDocumentoEmpresa() {
        return NroDocumentoEmpresa;
    }

    public void setNroDocumentoEmpresa(String nroDocumentoEmpresa) {
        NroDocumentoEmpresa = nroDocumentoEmpresa;
    }

    public String getNombreEmpresa() {
        return NombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        NombreEmpresa = nombreEmpresa;
    }

    public String getCargoEmpresa() {
        return CargoEmpresa;
    }

    public void setCargoEmpresa(String cargoEmpresa) {
        CargoEmpresa = cargoEmpresa;
    }

    public int getIdEmpresa() {
        return IdEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        IdEmpresa = idEmpresa;
    }

    public int getIdTienda() {
        return IdTienda;
    }

    public void setIdTienda(int idTienda) {
        IdTienda = idTienda;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }
}
