package com.gys.appgismovil.data.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.BaseColumns;

/**
 * Created by USER on 29/12/2017.
 */
public class BaseDatos extends SQLiteOpenHelper
{
    private static final String NOMBRE_BASE_DATOS = "gismovil.db";

    private static final int VERSION_ACTUAL = 1;

    private final Context contexto;

    public interface Tablas
    {
        String EMPRESA = "empresa";
        String TIENDA = "tienda";
        String USUARIO = "usuario";
    }

    interface Referencias
    {
        String ID_EMPRESA = String.format("REFERENCES %s(%s) ON DELETE NO ACTION ",
                Tablas.EMPRESA, Contrato.ContratoEmpresa.EMP_ID);

    }

    public BaseDatos(Context contexto)
    {
        super(contexto, NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
        this.contexto = contexto;
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
        if (!db.isReadOnly())
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            {
                db.setForeignKeyConstraintsEnabled(true);
            } else
            {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        // Empresa
        db.execSQL(String.format("CREATE TABLE %s (" +
                                "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s DATE NOT NULL," +
                                "%s DATE NOT NULL," +
                                "%s INTEGER NOT NULL," +
                                "%s INTEGER NOT NULL," +
                                "%s INTEGER NOT NULL)",
                                Tablas.EMPRESA,
                                Contrato.ContratoEmpresa.EMP_ID,
                                Contrato.ContratoEmpresa.EMP_DOCTIPO,
                                Contrato.ContratoEmpresa.EMP_DOCNRO,
                                Contrato.ContratoEmpresa.EMP_RAZONSOCIAL,
                                Contrato.ContratoEmpresa.EMP_TELEFONO,
                                Contrato.ContratoEmpresa.EMP_CORREO,
                                Contrato.ContratoEmpresa.EMP_DIRECCION,
                                Contrato.ContratoEmpresa.EMP_CIUDAD,
                                Contrato.ContratoEmpresa.EMP_ADMINISTRADOR,
                                Contrato.ContratoEmpresa.EMP_PLANAPP,
                                Contrato.ContratoEmpresa.EMP_PLANREGISTRO,
                                Contrato.ContratoEmpresa.EMP_PLANVENCIMIENTO,
                                Contrato.ContratoEmpresa.EMP_MAXUSER,
                                Contrato.ContratoEmpresa.EMP_CANUSER,
                                Contrato.ContratoEmpresa.EMP_ESTADO

                )
        );

        // Tienda
        db.execSQL(String.format("CREATE TABLE %s (" +
                                "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                                "%s TEXT NOT NULL," +
                                "%s INTEGER NOT NULL %s," +
                                "%s INTEGER NOT NULL)",
                                Tablas.TIENDA,
                                Contrato.ContratoTienda.TND_ID,
                                Contrato.ContratoTienda.TND_NOMBRE,
                                Contrato.ContratoTienda.TND_IDEMPRESA,
                                Referencias.ID_EMPRESA,
                                Contrato.ContratoTienda.TND_ESTADO
                )
        );

        // Usuario
        db.execSQL(String.format("CREATE TABLE %s (" +
                                "%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                                "%s INTEGER NOT NULL %s," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s TEXT NOT NULL," +
                                "%s INTEGER NOT NULL)",
                                Tablas.USUARIO,
                                Contrato.ContratoUsuario.USU_ID,
                                Contrato.ContratoUsuario.USU_IDEMPRESA,
                                Referencias.ID_EMPRESA,
                                Contrato.ContratoUsuario.USU_USERNAME,
                                Contrato.ContratoUsuario.USU_PASSWORD,
                                Contrato.ContratoUsuario.USU_NOMBRE,
                                Contrato.ContratoUsuario.USU_FUNCION,
                                Contrato.ContratoUsuario.USU_CORREO,
                                Contrato.ContratoUsuario.USU_AVATAR,
                                Contrato.ContratoUsuario.USU_ESTADO
                )
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.USUARIO);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.TIENDA);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.EMPRESA);

        onCreate(db);
    }
}
