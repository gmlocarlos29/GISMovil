package com.gys.appgismovil.data.model.entidades;

import com.gys.appgismovil.data.model.comun.Entidad;

/**
 * Created by USER on 04/09/2017.
 */
public class Cliente extends Entidad
{
    private String cli_id;
    private int cli_idempresa;
    private String cli_docnro;
    private String cli_nombre;
    private String cli_telefono;
    private String cli_correo;
    private String cli_direccion;
    private String cli_ciudad;
    private int cli_estado;

    public Cliente(int cli_idempresa,String cli_docnro,String cli_nombre,String cli_telefono,String cli_correo,String cli_direccion,
                    String cli_ciudad, int anio)
    {
        this.cli_idempresa = cli_idempresa;
        this.cli_docnro = cli_docnro;
        this.cli_nombre = cli_nombre;
        this.cli_telefono = cli_telefono;
        this.cli_correo = cli_correo;
        this.cli_direccion = cli_direccion;
        this.cli_ciudad = cli_ciudad;
        this.Anio = anio;
    }

    public String getCli_id() {
        return cli_id;
    }

    public void setCli_id(String cli_id) {
        this.cli_id = cli_id;
    }

    public int getCli_idempresa() {
        return cli_idempresa;
    }

    public void setCli_idempresa(int cli_idempresa) {
        this.cli_idempresa = cli_idempresa;
    }

    public String getCli_docnro() {
        return cli_docnro;
    }

    public void setCli_docnro(String cli_docnro) {
        this.cli_docnro = cli_docnro;
    }

    public String getCli_nombre() {
        return cli_nombre;
    }

    public void setCli_nombre(String cli_nombre) {
        this.cli_nombre = cli_nombre;
    }

    public String getCli_telefono() {
        return cli_telefono;
    }

    public void setCli_telefono(String cli_telefono) {
        this.cli_telefono = cli_telefono;
    }

    public String getCli_correo() {
        return cli_correo;
    }

    public void setCli_correo(String cli_correo) {
        this.cli_correo = cli_correo;
    }

    public String getCli_direccion() {
        return cli_direccion;
    }

    public void setCli_direccion(String cli_direccion) {
        this.cli_direccion = cli_direccion;
    }

    public String getCli_ciudad() {
        return cli_ciudad;
    }

    public void setCli_ciudad(String cli_ciudad) {
        this.cli_ciudad = cli_ciudad;
    }

    public int getCli_estado() {
        return cli_estado;
    }

    public void setCli_estado(int cli_estado) {
        this.cli_estado = cli_estado;
    }
}
